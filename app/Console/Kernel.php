<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
        Commands\EmailOffersToClient::class,
        Commands\EmailRequestsToProviders::class,
        Commands\EmailRequestsToProviders::class,
        Commands\UpdateExpiredRequests::class,
        Commands\LogCronJobs::class,
        Commands\RecruitProviders::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
//		send emails to users with the updated list of offers they received for their request
		$schedule->command('email:offers')->dailyAt('07:00');
//		send emails to providers with the latest list of new requests made in the last 24 hours
		$schedule->command('email:requests')->dailyAt('07:00');
//		make request status = expired on passing deadline
		$schedule->command('requests:expire')->dailyAt('08:00');
// 		checking that the server cron scheduler is working
// 		$schedule->command('log:crons')->everyMinute();
		
    }
}

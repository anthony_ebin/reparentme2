<?php 

namespace App\Console\Commands;

use Illuminate\Console\Command;

class LogCronJobs extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $signature = 'log:crons';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This command will add one line to the log file proofing that cron is working.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		\Log::info('Cron job was run @' . \Carbon\Carbon::now());
	}


}
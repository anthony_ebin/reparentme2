<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use DB;
use Carbon\Carbon;
use App\Invitation;
use App\Role;

class EmailRequestsToProviders extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $signature = 'email:requests';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Email number of new requests made to providers and admin';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
//		get count of anon requests made in the last 24 hours
		$requests = Invitation::where('created_at', '>=', Carbon::now()->subHours(24));

		$providers = Role::therapists()->get();

//		if there are more than 0 requests made in the last 24 hours, send an email to each provider
		if ($requests->count()>0)
		{
			foreach ($providers as $provider) {
				Mail::queue('emails.newrequests', ['user' => $provider], function ($m) use ($provider) {
	                $m->from('noreply@reparent.me', 'ReparentMe');
					
					$m->to($provider['email'])->subject('Clients are looking for you!');
				});
			}

		}

	}

}
<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use DB;

class EmailOffersToClient extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $signature = 'email:offers';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Email updated list of offers made to client for his request';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
//		we need name of user and request id
//		select user id and request id from requests table wherever the request has at least one offer today
		$users_with_new_offers = DB::select(DB::raw('select invitations.id, users.email from invitations
		 											 left outer join offers
		 											 on invitations.id = offers.invitation_id
		 											 left outer join users
		 											 on invitations.user_id = users.id
		 											 where DATE(offers.created_at) >= now() - INTERVAL 1 DAY
		 											 group by invitations.id
		 											 '));

		$users_with_new_offers = json_decode(json_encode($users_with_new_offers), true);

//		if user has more than 0 offers only then send him an email
		if (count($users_with_new_offers)>0)
		{
			foreach ($users_with_new_offers as $user_with_new_offers) {
				Mail::queue('emails.newoffer', $user_with_new_offers, function ($m) use ($user_with_new_offers) {
	                $m->from('noreply@reparent.me', 'ReparentMe');
	                
					$m->to($user_with_new_offers['email'])->subject('You have new offers on your request');
				});
			}
		}

	}

}
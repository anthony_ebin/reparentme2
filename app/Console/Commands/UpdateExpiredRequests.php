<?php 

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;
use Carbon\Carbon;
use App\InvitationStatus;

class UpdateExpiredRequests extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $signature = 'requests:expire';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update expired requests.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$openid = InvitationStatus::where('description', 'Open')->first()->id;
		$closeid = InvitationStatus::where('description', 'Expired')->first()->id;
		
		DB::table('invitations')
			->where('invitation_statuses_id', $openid)
			->where('deadline', '<=', Carbon::now())
			->update(['invitation_statuses_id' => $closeid]);
	}


}
<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;

class RecruitProviders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:recruit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email to recruit providers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $target = ['marketing@reparent.me'];
		Mail::queue('emails.recruit', $target, function ($m) use ($target) {
            $m->from('recruitment@reparent.me', 'ReparentMe');
            
			$m->to($target[0])->subject('ReparentMe - Client Matching Service');
		});
    }
}

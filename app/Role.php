<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }

    public function givePermissionTo(Permission $permission)
    {
        return $this->permissions()->save($permission);
    }

    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * @param $query - instance of Role
     * @return mixed collection of users who are providers
     */
    public function scopeTherapists($query)
    {
        return $query->whereName('therapist')->first()->users();
    }
    

}

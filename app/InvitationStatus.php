<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class InvitationStatus extends Model
{
    /**
     * Specify the database table used by the model because otherwise the default table used is invitation_statuss not invitation_statuses
     *
     * @var string
     */
    protected $table = 'invitation_statuses';
    
    /**
     * An invitation status can have many invitations
     */
    public function invitations()
    {
        return $this->hasMany('App\Invitation', 'invitation_statuses_id');
    }
    
    /**
     * reusable code (query scope) to quickly select requests which are still alive
     * @param $query
     */
    public function scopeGetAliveRequests($query)
    {
        $query->whereDescription('Open')->first()->request()->where('deadline', '>=', Carbon::now());
    }
}

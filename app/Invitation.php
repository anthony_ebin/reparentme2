<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Invitation extends Model
{
    protected $fillable = [
        'description',
        'deadline',
        'budget_range_min',
        'budget_range_max',
        'invitation_statuses_id',
    ];
    
    // Date Mutators
    protected $dates = ['deadline'];
    
    /**
     * Mutator to convert deadline in input to a Carbon instance
     *
     * @param $date
     */
    public function setDeadlineAttribute($date)
    {
        $this->attributes['deadline'] = Carbon::parse($date);
    }
    
    /**
     * A invitation belongs to one invitation status
     * http://stackoverflow.com/questions/26178049/attaching-a-hasone-model-to-another-laravel-eloquent-model-without-specifying-id
     * http://stackoverflow.com/questions/27828476/laravel-save-one-to-many-relationship
     */
    public function invitationStatus()
    {
        return $this->belongsTo('App\InvitationStatus', 'invitation_statuses_id');
    }
    
    /**
     * An anon request belongs to a user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
	
	/**
     * A request can have many offers
     */
    public function offers()
    {
        return $this->hasMany('App\Offer');
    }
	
	/**
     * reusable code (query scope) to quickly select requests which are still alive
     * @param $query
     */
    public function scopeIsAlive($query)
    {
        $query->where('invitation_statuses_id', InvitationStatus::whereDescription('Open')->first()->id)->where('deadline', '>=', Carbon::now());
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return Redirect::to('home');
});

Route::auth();

Route::get('/home', 'HomeController@index');

// url for registerprovider should store session data and redirect
Route::get('/registerprovider', function() {
    
    // Clear session first
    if(Session::has('registertherapist')) Session::forget('registertherapist');
    Session::push('registertherapist','true');
    
    return Redirect::to('register');
    
});


// Route::post('provider/message', 'TherapistController@write');
Route::post('provider/{id}/message', 'TherapistController@write');
Route::resource('provider', 'TherapistController');
Route::get('/providerprofile', 'TherapistController@edit');


Route::get('/profile', 'HomeController@editProfile');
Route::post('/profile', 'HomeController@store');

Route::resource('requests', 'InvitationController');
Route::get('my_requests', function(){
	$invites = App\Invitation::with('offers')->where('user_id', Auth::User()->id)->latest('created_at')->paginate(10);
	return view('invitations.history', compact('invites'));
});
// Route to CRUD provider offers for requests
Route::resource('offers', 'OffersController');

Route::get('terms', function(){
    return view('terms');
});
Route::get('about', function(){
    return view('about');
});
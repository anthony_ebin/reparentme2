<?php

namespace App\Http\Middleware;

use Closure;
use App\Offer;

class OwnerCannotMakeOfferOnOwnRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $referring_url = $request->session()->get('_previous')["url"]; //server('HTTP_REFERER');
        $referring_request_id = explode('/', $referring_url);
//        the 4th element of the url array has the request id
        $referring_request_id = $referring_request_id[4];
        // dd($referring_request_id);

        $send_to_request_id = $request->invitation_id;

        if ($referring_request_id<>$send_to_request_id) {
            flash()->overlay('Sneaky!', 'NO!');

            return redirect('profile');
        }

        return $next($request);
    }
}

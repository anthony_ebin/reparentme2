<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class OnlyOneActiveRequestAtATime
{

    /**
     * Handle an incoming request and validate that the user can only have one active public request at a time
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()) return $next($request);

        $active_request = $request->user()->invitations()->isAlive()->first();

        if(isset ($active_request))
        {
            flash()->overlay('You can only create one active request at a time.', 'Sorry!');

            return redirect('requests/'.$active_request->id);
        } else return $next($request);

    }
    
}
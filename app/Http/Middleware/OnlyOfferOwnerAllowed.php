<?php

namespace App\Http\Middleware;

use Closure;
use App\Offer;

class OnlyOfferOwnerAllowed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//		get id from url
        $url = $request->decodedPath();
        $id = explode('/', $url);
        $offer_owner_id = Offer::find($id[1])->therapist_id;

        $user_therapist_id = $request->user()->therapist()->first()->id;

        if ($offer_owner_id<>$user_therapist_id) {
            flash()->overlay('You do not own that offer.', 'Sorry!');

            return redirect('profile');
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use App\InvitationStatus;
use App\Invitation;
use Auth;

class OnlyRequestOwnerAndProvidersAllowed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//		get id from url
			$url = $request->decodedPath();
			$id = explode('/', $url);
			$service_request = Invitation::find($id[1]);
			$service_request_owner_id = $service_request->user_id;


			$user_id = $request->user()->id;

//			Owner is allowed to see his request
			if ($service_request_owner_id==$user_id) return $next($request);

// 		    If user has super privileges then let him through
			if($request->user()->hasPermission('super')) return $next($request);

//			If the user visiting this page is a provider and the service request is active then go ahead
			if ($service_request->invitationStatus()->first()->description=='Open'){
				if ($request->user()->hasPermission('view_public_invitations')) 
				{
					return $next($request);
				}
			}

//			If the user visiting this page is a provider but the service request is closed then allow passage if the user has made an offer
			if ($service_request->invitationStatus()->first()->description=='Closed'){
				$providers_haystack = $service_request->offers->lists('therapist_id')->all();
//				dd(in_array(Auth::User()->id, $providers_haystack) );
				if ( in_array(Auth::User()->therapist()->first()->id, $providers_haystack) )
				{
					return $next($request);
				}
			}
			
            // for any other reason, reject the request
			flash()->overlay('You are not authorized to view that request', 'Sorry!');

			return redirect('requests');    
        
    }
}

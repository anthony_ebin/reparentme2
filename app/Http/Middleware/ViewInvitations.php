<?php

namespace App\Http\Middleware;

use Closure;

class ViewInvitations
{
    /**
     * Handle an incoming request. and validate that it is from a permitted user, usually a provide
     * Only certain people, ususally providers/therapists can see public requests
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//      If user is requesting to see his own request then let him
        if($request->my_request==1) return $next($request);

//      If user has permission to view requests then let him through
        if($request->user()->hasPermission('view_public_invitations')) return $next($request);

        return redirect('profile');
    }
}

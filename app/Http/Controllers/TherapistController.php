<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Question;
use Auth;
use App\Therapist;
use DB;
use App\Role;
use App\User;
use Illuminate\Pagination\Paginator;
use Mail;
use Session;

class TherapistController extends Controller
{
    
    /**
     * Run middleware to authenticate users
     */
    public function __construct()
    {
		$this->middleware('auth', ['only'=>['edit', 'store']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $client = Auth::user();
        $budget = $request->budget;
        
        $scorefield = '';
        $scorejoin = '';
				
        $roleid = Role::whereName('therapist')->first()->id;
		$whereclause = "WHERE role_id = $roleid ";
		
        if (isset($request->sortby)) {
        	switch ($request->sortby) {
        		case 'best':
        			$orderbyquery = 'ORDER BY score DESC';
        			break;
        		case 'highprice':
        			$orderbyquery = 'ORDER BY standard_hourly_rate_min DESC';
        			break;
        		case 'lowprice':
        			$orderbyquery = 'ORDER BY standard_hourly_rate_min ASC';
        			break;
        		default:
        			$orderbyquery = 'ORDER BY score DESC';
        			break;
        	}
        } else $orderbyquery = '';
				
        if(isset($budget))
        {
        	$whereclause = "AND standard_hourly_rate_min <= $budget";
        }

        //get total score
        if (isset($client)) {
            $clientid= $client->id;
            $totalscore = DB::select(DB::raw('select sum(importance) as imp from (select importance 
                                                from answer_user 
                                                inner join answers on answers.id = answer_user.answer_id
                                                where user_id= :clientid group by question_id) as distinctAnswers'), ['clientid'=>$clientid])[0]->imp;
            		
    		$scorefield = ', IFNULL(scores.score,0) as score';
    		$scorejoin = 'LEFT JOIN (SELECT
                    					SUM(importance)/:totalscore as score,
                    					answer_therapist.therapist_id as therapist_id
                    				FROM answer_user
                    					INNER JOIN answers on answers.id = answer_user.answer_id
                    					INNER JOIN answer_therapist on answers.id = answer_therapist.answer_id 
                    				WHERE answer_user.user_id = :clientid
                    				GROUP BY answer_therapist.therapist_id) scores on scores.therapist_id =therapists.id';
        				
    		$params = ['clientid'=>$clientid, 'totalscore'=>$totalscore];
        } else {
            $totalscore = 0;
            $params = [];
        }
        
        
        $therapistsQuery = "SELECT 
                            	therapists.id as therapist_id,
                            	users.id as user_id,
                            	name,
                            	email,
                            	account_summary,
                            	first_session_rate, 
                            	first_session_length, 
                            	standard_hourly_rate_min, 
                            	standard_hourly_rate_max
                            	$scorefield
                            FROM therapists 
                            	INNER JOIN users on users.id=therapists.user_id
                            	LEFT JOIN role_user on users.id = role_user.user_id
                            	$scorejoin
                            $whereclause
                            GROUP BY users.id
                            $orderbyquery";
                            
		$therapists = DB::select(DB::raw($therapistsQuery), $params);
		
// 		dd(DB::raw($therapistsQuery), ['clientid'=>$clientid, 'totalscore'=>$totalscore]);
    		
        // $therapists = new Paginator($therapists, '10');

		return view('therapists.index', compact('therapists', 'totalscore'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store therapist profile details for current user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'account_summary' => 'bail|required|max:20000|min:20',
			'first_session_rate'=> 'bail|required|numeric|min:0|max:200',
			'first_session_length'=> 'bail|required|numeric|min:20|max:120',
//			Min should always be less than max
			'standard_hourly_rate_min'=> 'bail|required|numeric|between:0,'.$request->standard_hourly_rate_max,
			'standard_hourly_rate_max'=> 'bail|required|numeric|between:'.$request->standard_hourly_rate_min.',200',
			'terms' => 'required'
        ]);
        
        // Clear session first
        if(Session::has('registertherapist')) Session::forget('registertherapist');
		
        // dd($request);
        // update basic therapist details if therapist already exists, else create
        $user = Auth::user()->therapist()->first();
        if ($user) {
            $user->account_summary = $request->account_summary;
            $user->first_session_rate = $request->first_session_rate;
            $user->first_session_length = $request->first_session_length;
            $user->standard_hourly_rate_min = $request->standard_hourly_rate_min;
            $user->standard_hourly_rate_max = $request->standard_hourly_rate_max;
        } else {
            $user = Auth::user()->therapist()->create([
                'account_summary'=> $request->account_summary,
                'first_session_rate'=> $request->first_session_rate,
                'first_session_length'=> $request->first_session_length,
                'standard_hourly_rate_min'=> $request->standard_hourly_rate_min,
                'standard_hourly_rate_max'=> $request->standard_hourly_rate_max
                ]);
        }
        
        $user->save();
        
        // mark role as therapist
        if (!Auth::user()->hasRole('therapist')) {
            Auth::user()->assignRole('therapist');
        }
		
        $therapist= Auth::user()->therapist()->first();
        // update therapist answers to questions
        $questions = Question::all();
        
        // for each question from the question model use the question id to get the therapist's answers from the request data
        // then attach the answer id to the therapists model, i.e. store the answer id with question id in the answer_therapist table
        $answers = [];
        foreach ($questions as $question) {
            $questionid = $question->id;
            if (null !== $request->$questionid) {
                $answers[] = $request->$questionid;
            }
        }
        $therapist->answers()->sync($answers);
        
        // email to provider informing him that he is a provider
        if (getenv('APP_ENV')!='testing') {
            $user = $therapist->user()->first();
            Mail::queue('emails.approvedprovider', ['user' => $user], function ($m) use ($user) {
                $m->from('noreply@reparent.me', 'ReparentMe');
    
                $m->to($user->email, $user->name)->subject('You have joined as a provider on ReparentMe!');
            });
            
            Mail::queue('emails.approvedproviderToAdmin', ['user' => $user], function ($m) {
                $m->from('manager@reparent.me', 'via ReparentMe');
    
                $m->to('anthonyebin@gmail.com', 'Anthony Ebin')->subject('New Provider on ReparentMe!');
            });
        }

        return redirect('requests');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $arr = explode("-", $slug, 2);
		$user_id = $arr[0];

//		If user manually types in $id he can see the client's profile page
// 		but it will mostly be empty so that should be ok
        $therapist = User::with('therapist')->findOrFail($user_id);
                
        if($therapist->hasRole('therapist'))
        {
            $questions = Question::all();
    
            return view('therapists.show', compact('therapist', 'questions'));
        } else App::abort(403, 'Unauthorized action.'); 
        
    }

    /**
     * Show the form for editing the therapist profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        // If user is pending approval as a provider or is a provider then redirect him to articles, else let him submit a request to become a provider
// 		if ((Auth::User()->hasRole('provider')) || (Auth::User()->hasRole('pending-provider')))
// 		{
// 			return redirect('articles');
// 		}
        
        $questions = Question::all();
        // pull this therapist's answers from the db and pass to view
        // maybe a right join with questions to get all questions or pass questions separately
        
        $therapist = Therapist::where('user_id', Auth::user()->id)->first();
        
        return view('therapists.edit', compact('questions', 'therapist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Message being sent to provider
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id id of provider
     * @return \Illuminate\Http\Response
     */
    public function write(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $sender = $request->sender_email;
        $body = $request->message_body;
        
        Mail::queue('emails.contactprovider', ['user' => $user, 'body'=>$body, 'sender'=>$sender], function ($m) use ($user, $sender) {
            $m->from($sender, "$sender via ReparentMe");

            $m->to($user->email, $user->name)->subject('You have mail via ReparentMe!');
        });
        
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

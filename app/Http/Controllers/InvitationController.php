<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use App\Http\Requests\InvitationRequest;
use Auth;
use App\InvitationStatus;
use App\Invitation;
use App\Offer;
use Mail;

class InvitationController extends Controller
{
    /**
	 * Run middleware to authenticate users
	 */
	public function __construct()
	{
//		Only therapist can see published requests
		$this->middleware('view_invitations', ['only'=>'index']);
//		Client can only have one active public request at a time
		$this->middleware('singly-active-request', ['only'=>'create']);
		$this->middleware('owner-or-bidded-provider', ['only'=>'show']);
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$sortby = $request->sortby;
		$order = $request->order;

//		eager loading using with offers
		if ($sortby && $order) {
			$invites = Invitation::isAlive()->with('offers')->orderBy($sortby, $order);
		} else {
			$invites = Invitation::isAlive()->with('offers')->latest('created_at');
		}

		$invites = $invites->paginate(10);

		return view('invitations.index', compact('invites', 'sortby', 'order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (Session::has('anonRequest')){
			$anonRequest = Session::get('anonRequest');
			return view('invitations.create', compact('anonRequest'));
		}
		return view('invitations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InvitationRequest $request)
    {
		if (!Auth::check()){
//			Clear session first
			if(Session::has('anonRequest'))	Session::forget('anonRequest');

//			Add each field to a session variable
			Session::push('anonRequest',$request->description);
			Session::push('anonRequest',$request->deadline);
			Session::push('anonRequest',$request->budget_range_min);
			Session::push('anonRequest',$request->budget_range_max);

			return view('auth.register');
		}

//		Clear session on authorized submit
		if(Session::has('anonRequest'))	Session::forget('anonRequest');

		$createdRequest = Auth::user()->invitations()->create($request->all());

		$createdRequest->invitationStatus()->associate(InvitationStatus::whereDescription('Open')->firstOrFail());

		$createdRequest->save();

// 		flash()->overlay('Your request has been published to our providers. You will be contacted with offers soon!', 'Good Job!');

//		notify Anthony for the first 100 requests
		if (getenv('APP_ENV')!='testing') {
			if ($createdRequest->id < 100){
				// dd($createdRequest->id);
	            Mail::queue('emails.newrequestsToAdmin', ['id' => $createdRequest->id], function ($m) {
	                $m->from('noreply@reparent.me', 'ReparentMe');
	    
	                $m->to('anthonyebin@gmail.com', 'Anthony Ebin')->subject('New Request on ReparentMe!');
	            });
			}
		}

		return redirect('requests/'.$createdRequest->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$invite = Invitation::with('invitationStatus')->findOrFail($id);

		$user = Auth::User();
		$is_provider = $user->hasRole('therapist');
//		Provider cannot submit offer for his own request and only owner can see bids
		$is_owner_of_request = ($invite->user->id == $user->id);
//		Provider can only make one bid per request, i.e. if a bid already exists he cannot create another one
		if ($is_provider) {
			$existing_bid_for_this_therapist = Offer::where('invitation_id', $id)->where('therapist_id', $user->therapist()->first()->id)->first();
		} else $existing_bid_for_this_therapist = false;
		
		$existing_bids_for_this_request = Offer::where('invitation_id', $id)->get();

		return view('invitations.show', compact('invite', 'is_provider', 'is_owner_of_request', 'existing_bids_for_this_request', 'existing_bid_for_this_therapist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$invitation = Invitation::find($id);

		$invitation->invitationStatus()->associate(InvitationStatus::whereDescription('Closed')->firstOrFail()); 

		$invitation->save();

		// flash()->overlay('Request closed, you will no longer receive offers on this request.', 'Done!');

		return redirect('requests/'.$id);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MakeOfferRequest;

use App\Http\Requests;
use Auth;
use App\Offer;
use Carbon\Carbon;

class OffersController extends Controller
{
    /**
	 * Run middleware to authenticate users
	 */
	public function __construct()
	{
//		only members can view offers
		$this->middleware('auth');
//		only offer owners can do anything with his offers
		$this->middleware('offer-owner', ['only'=>['destroy', 'show']]);
//		if the user is owner of the request then he should not be allowed to make an offer
		$this->middleware('no-request-owner', ['only'=>'store']);
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$offers = Offer::isMine()->with('invitation')->latest('created_at')->paginate(20);

		return view('offers.index', compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MakeOfferRequest $request)
    {
        $request['therapist_id'] = Auth::User()->therapist()->first()->id;

		Offer::create($request->all());

// 		flash('Your offer has been submitted to the client.', 'success');

		return redirect('requests/'.$request->invitation_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$offer = Offer::isMine()->findOrFail($id);

		$current_date_time = new Carbon();

		$is_active = ($offer->invitation->invitationStatus()->first()->description == 'Open');

		return view('offers.show', compact('offer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$offer = Offer::find($id);

//		If you are the owner:
		$offer->delete();

// 		flash()->overlay('Offer deleted.', 'Done!');

		return redirect('requests');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Question;
use Auth;
use App\InvitationStatus;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		$this->middleware('auth', ['only'=>['editProfile', 'dashboard', 'store']]);
    }

    /**
     * Show the application homepage.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Logged in client cant see guest home page - he's taken to dashboard. This is to prevent the guest form from being used
        if (Auth::check()) {
            return redirect('profile');
        }
        $question = Question::whereQuestion('Importance of religion or spirituality')->first();
        $answers = $question->answers()->get();
        
        return view('home', compact('question', 'answers'));
    }
    
    /**
     * Edit client profile, answer questions
     * 
     */
    public function editProfile()
    {
        $questions = Question::all();
        
        $client = Auth::user();
        
    	if (isset($client))
    	{
    		$user_active_request = $client->invitations()->where('invitation_statuses_id', InvitationStatus::whereDescription('Open')->first()->id)->first();
    
    		if ($user_active_request <> null) {
    			$user_active_request_id = $user_active_request->id;
    		}
    
    		return view('clientProfile', compact('questions', 'client', 'user_active_request_id'));
    	} else App::abort(403, 'Unauthorized action.');
        
    }    
    
    /**
     * Store client profile details for current user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        // validation on client answers
        
        // store answers in answer_client
        $client=Auth::user();
        $questions = Question::all();
        
        //detach existing answers
        $client->answers()->detach();
        
        // for each question from the question model use the question id to get the therapist's answers from the request data
        // then attach the answer id to the therapists model, i.e. store the answer id with question id in the answer_therapist table
        foreach ($questions as $question) {
            $questionid = $question->id;
            $importanceid = $questionid.'_importance';
            $importanceword =  isset($request->$importanceid)?$request->$importanceid:'none';
            
            // classify importance
            switch ($importanceword) {
                case "none":
                    $importance=0;
                    break;
                case "little":
                    $importance=1;
                    break;
                case "somewhat":
                    $importance=10;
                    break;
                case "very":
                    $importance=50;
                    break;
                default:
                    $importance=0;
            } 
            
            // if question has been answered
            if (isset($request->$questionid)) {
                // if question is a checkbox then store each answer
                if ($question->checkbox) {
                    $answerids = $request->$questionid;
                    // if count of answer ids is same as number of answers in this question then importance is none, i.e if client selects all answers then he doesn't have a preference
                    if (count($question->answers)==count($answerids)) {
                        $importance=0;
                    }
                    foreach ($answerids as $answerid) {
                        // dd($answerid);
                        $client->answers()->attach($answerid, ['importance' => $importance]);
                    }
                } else {            
                    $answerid = $request->$questionid;
                    $client->answers()->attach($answerid, ['importance' => $importance]);
                }

            }
        }
        
        
        return back();
    }
    
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Carbon\Carbon;
use Auth;
use App\InvitationStatus;

class InvitationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		$dt = new Carbon();
//		For some reason if I don't use a new instance of Carbon $minDeadline will be reset to $maxDeadline
		$dt1 = new Carbon();
		$minDeadline = $dt1;

		//deadline should not be more than two weeks
		$maxDeadline = $dt->addDays(14)->format('Y-m-d');

		$this->invitation_statuses_id=InvitationStatus::whereDescription('Open')->first()->id;
    
        // custom validator in Providers folder under CustomValidator.php for onerequestatatime
		$rules = [
			'description' => 'required|max:2000|min:20|onerequestatatime',
			'deadline' => 'required|date|before:'.$maxDeadline.'|after:'.$minDeadline,
			'budget_range_min' => 'numeric|between:0,'.$this->budget_range_max,
			'budget_range_max' => 'numeric|between:'.$this->budget_range_min.',200',
		];

		return $rules;
    }
    
	public function messages()
	{
		return [
			'budget_range_min.between' => 'The minimum rate must be greater than 0 and less than the maximum rate.',
			'budget_range_max.between' => 'The maximum rate must be greater than 0 and less than 200 USD.',
			'description.onerequestatatime' => 'There can only be one active request at a time.'
		];
	}
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MakeOfferRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		return [
			'hourly_price'=> 'required|numeric|min:0|max:200',
			'cover_letter' => 'required|max:2000|min:20',
		];
    }
}

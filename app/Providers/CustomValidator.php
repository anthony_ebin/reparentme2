<?php 

namespace App\Providers;

use Illuminate\Validation\Validator;
use Auth;

/**
 * Class CustomValidator
 * https://laracasts.com/discuss/channels/general-discussion/custom-validation-function-in-laravel-5
 * @package App\Services\Validation
 */
class CustomValidator extends Validator
{

    /**
     * Client can only have one active public request at a time
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    public function validateOnerequestatatime($attribute, $value, $parameters)
    {
//        get any existing active request of user
        if (Auth::check()){
            $active_request = Auth::User()->invitations()->isAlive()->first();
            if(isset($active_request))
            {
                return false;
            }
        }
        return $value;
    }

}
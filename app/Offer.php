<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{

    protected $fillable = [
        'hourly_price',
        'invitation_id',
        'therapist_id',
        'cover_letter',
    ];


    /**
     * An offer belongs to one invitation
     */
    public function invitation()
    {
        return $this->belongsTo('App\Invitation');
    }
    
    /**
     * An offer belongs to one therapist
     */
    public function therapist()
    {
        return $this->belongsTo('App\Therapist');
    }    
    
    /**
     * reusable code (query scope) to quickly select offers which are mine
     * @param $query
     */
    public function scopeIsMine($query)
    {
        $query->where('therapist_id', Auth::User()->therapist()->first()->id);
    }
}

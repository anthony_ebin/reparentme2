<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * Get the question that owns the answer.
     */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }
    
    /**
     * The therapists that belong to the answer i.e. the therapists that have chosen this answer.
     */
    public function therapists()
    {
        return $this->belongsToMany('App\Therapist');
    }
    
    /**
     * The users that belong to the answer i.e. the users that have chosen this answer.
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('importance');
    }
}

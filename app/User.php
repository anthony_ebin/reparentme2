<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasRoles;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * 
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
	public function setNameAttribute($value)
	{
		$this->attributes['name'] = ucwords($value);
	}
    
    /**
     * Get the therapist record associated with the user.
     */
    public function therapist()
    {
        return $this->hasOne('App\Therapist');
    }
    
    /**
     * The answers that belong to the client.
     */
    public function answers()
    {
        return $this->belongsToMany('App\Answer')->withPivot('importance');
    }
    
    
	/**
	 * A user can have many anonymous therapy requests
	 */
	public function invitations()
	{
		return $this->hasMany('App\Invitation');
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Therapist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_summary', 'first_session_rate', 'first_session_length', 'standard_hourly_rate_min', 'standard_hourly_rate_max'
    ];
    
    /**
     * should append these properties to the model
     * http://stackoverflow.com/questions/17232714/add-a-custom-attribute-to-a-laravel-eloquent-model-on-load
     */
    // protected $attributes = array('score');
    protected $appends = array('score');
    
    /**
     * Get score of this therapist
     * 
     */ 
    public function getScoreAttribute()
    {
        $client = Auth::user();

        if (isset($client)) {
            $totalscore = DB::select(DB::raw('select sum(importance) as imp from (select importance 
                                                from answer_user 
                                                inner join answers on answers.id = answer_user.answer_id
                                                where user_id= :clientid group by question_id) as distinctAnswers'), ['clientid'=>$client->id])[0]->imp;
        } else $totalscore = 0;
        
        if (isset($client)) {
            $score = DB::select(DB::raw("select sum(importance)/:totalscore as imp
                                            from answer_user 
                                            inner join answers on answers.id = answer_user.answer_id 
                                            inner join answer_therapist on answers.id = answer_therapist.answer_id
                                            where answer_user.user_id = :clientid AND answer_therapist.therapist_id = :therapistid"),
                                            ['clientid' => $client->id, 'totalscore' => $totalscore, 'therapistid' => $this->id])[0]->imp;
        } else {
            $score=0;
        }
        // sprintf("%.2f%%", $x * 100) 
        return $score;
    }
    
    /**
     * Get the user record that is this therapist.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    /**
     * A therapist can have many offers
     */
    public function offers()
    {
        return $this->hasMany('App\Offer');
    }
	
    /**
     * The answers that belong to the therapist.
     */
    public function answers()
    {
        return $this->belongsToMany('App\Answer');
    }
}

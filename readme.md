# ReparentMe

Reparent.me was a self funded web application and business built using the Laravel framework for PHP.
The name is based on a concept in psychology, idealizing therapy as a process of reparenting the client. 
The application targeted a potential niche in the marketplace to help clients find the perfect therapist online using a proprietary algorithm inspired by the **OKCupid model** of matching romantic partners. 
![Imgur](https://i.imgur.com/gJc8zq1l.png)

The therapist answers a series of questions that is important to the average client in identifying an ideal provider. I used the studies done by professor Kenneth Frankel ([tmatch.org](http://tmatch.org)) to develop a series of questions that suit this model. 
The client visiting the site gets to answer the same series of questions and when browsing for therapists, is shown a match-score against each provider:
![Imgur](https://i.imgur.com/yJJAaLql.png)
![Imgur](https://i.imgur.com/alY5HIwl.png)

The application also offers functionality to anonymously post a request and let providers bid on the request. This is similar to the job bidding process on sites like upwork.com or freelancer.com
![Imgur](https://i.imgur.com/gGy16VAl.png)

The application was fully responsive, using CSS flexbox. The entire design was done in pure CSS without resorting to frameworks like Bootstrap or Foundation.
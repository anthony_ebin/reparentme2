<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('offers', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->integer('invitation_id')->unsigned();
			$table->foreign('invitation_id')->references('id')->on('invitations')->onDelete('cascade');
			
			$table->integer('therapist_id')->unsigned();
			$table->foreign('therapist_id')->references('id')->on('therapists')->onDelete('cascade');
			
			$table->tinyInteger('hourly_price')->unsigned();
			$table->text('cover_letter');
			$table->boolean('is_accepted');
			$table->timestamps();
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('offers', function(Blueprint $table)
		{
			$table->dropForeign('offers_therapist_id_foreign');
			$table->dropForeign('offers_invitation_id_foreign');
		});

		Schema::drop('offers');
	}
}

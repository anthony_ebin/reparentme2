<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerTherapistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_therapist', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('therapist_id')->unsigned();
            $table->foreign('therapist_id')->references('id')->on('therapists')->onDelete('cascade');
            
            $table->integer('answer_id')->unsigned();
            $table->foreign('answer_id')->references('id')->on('answers')->onDelete('restrict');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answer_therapist', function(Blueprint $table)
		{
			$table->dropForeign('answer_therapist_therapist_id_foreign');
			$table->dropForeign('answer_therapist_answer_id_foreign');
		});
		
        Schema::drop('answer_therapist');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitation_statuses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('description');
			$table->timestamps();
		});

		Schema::create('invitations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->date('deadline');
			$table->text('description');
			$table->smallInteger('budget_range_min')->unsigned();
			$table->smallInteger('budget_range_max')->unsigned();
			$table->integer('invitation_statuses_id')->unsigned()->index()->nullable();
			$table->foreign('invitation_statuses_id')->references('id')->on('invitation_statuses')->onDelete('restrict');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('invitations', function(Blueprint $table)
		{
			$table->dropForeign('invitations_user_id_foreign');
			$table->dropForeign('invitations_invitation_statuses_id_foreign');
		});
		Schema::drop('invitations');
		Schema::drop('invitation_statuses');
    }
}

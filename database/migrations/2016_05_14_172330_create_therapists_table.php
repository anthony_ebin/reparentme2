<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTherapistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('therapists', function (Blueprint $table) {
            $table->increments('id');
            
            // one to one - user and therapist
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->text('account_summary');
			$table->smallInteger('first_session_rate')->unsigned();
			$table->tinyInteger('first_session_length')->unsigned()->default('20');
			$table->smallInteger('standard_hourly_rate_min')->unsigned();
			$table->smallInteger('standard_hourly_rate_max')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('therapists', function(Blueprint $table)
		{
			$table->dropForeign('therapists_user_id_foreign');
		});
        Schema::drop('therapists');
    }
}

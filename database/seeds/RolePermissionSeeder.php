<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RolePermissionSeeder extends Seeder {
//DO NOT RUN IN PRODUCTION
    public function run()
    {
        DB::table('roles')->delete();
        DB::table('permissions')->delete();
        DB::table('permission_role')->delete();

        if (null == Role::first())
        {

            Role::create(['name' => 'manager', 'label' => 'Site Manager']);
            Role::create(['name' => 'client', 'label' => 'Registered Client']);
            Role::create(['name' => 'pending-therapist', 'label' => 'User awaiting approval to become a provider']);
            Role::create(['name' => 'therapist', 'label' => 'Provider']);
            Role::create(['name' => 'premium_therapist', 'label' => 'Premium Provider']);
            Role::create(['name' => 'guest', 'label' => 'Guest']);

        }

        if (null == Permission::first())
        {

            Permission::create(['name' => 'view_public_invitations', 'label' => 'Allows user to access public invitations via requests route']);
            Permission::create(['name' => 'super', 'label' => 'Allows user to access anything and everything']);

            Role::whereName('manager')->first()->givePermissionTo(Permission::whereName('view_public_invitations')->first());
            Role::whereName('manager')->first()->givePermissionTo(Permission::whereName('super')->first());
            Role::whereName('therapist')->first()->givePermissionTo(Permission::whereName('view_public_invitations')->first());

        }


    }

}
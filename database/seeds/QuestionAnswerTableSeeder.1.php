<?php

use Illuminate\Database\Seeder;
use App\Question;
use App\Answer;

class QuestionAnswerTableSeeder extends Seeder {

    public function run()
    {
        DB::table('questions')->delete();
        DB::table('answers')->delete();

        if (null == Question::first() && null == Answer::first())
        {
            $q = Question::create(['question' => 'Therapist Age', 'category' => 'Demographic', 'checkbox' => true, 'question_client' => "I prefer my provider's age to be...", 'question_therapist' => 'I fall into the following age bracket:']);
            $q->answers()->save(new Answer(['answer' => '25 to 35']));
            $q->answers()->save(new Answer(['answer' => '35 to 45']));
            $q->answers()->save(new Answer(['answer' => '45 to 55']));
            $q->answers()->save(new Answer(['answer' => 'Over 55']));
            
            $q = Question::create(['question' => 'Sex', 'category' => 'Demographic', 'checkbox' => true, 'question_client' => 'I prefer my provider should be a...', 'question_therapist' => 'My gender:']);
            $q->answers()->save(new Answer(['answer' => 'Male']));
            $q->answers()->save(new Answer(['answer' => 'Female']));

            $q = Question::create(['question' => 'Race', 'category' => 'Demographic', 'checkbox' => true, 'question_client' => "I prefer my provider's ethnicity to be...", 'question_therapist' => 'My ethnicity:']);
            $q->answers()->save(new Answer(['answer' => 'Caucasian']));
            $q->answers()->save(new Answer(['answer' => 'Black']));
            $q->answers()->save(new Answer(['answer' => 'Asian']));
            $q->answers()->save(new Answer(['answer' => 'Hispanic']));
            $q->answers()->save(new Answer(['answer' => 'Other']));
            $q->answers()->save(new Answer(['answer' => 'Mixed']));
            
            
            $q = Question::create(['question' => 'Sexual Orientation', 'category' => 'Demographic', 'checkbox' => true, 'question_client' => "I prefer my provider's sexual orientation to be...", 'question_therapist' => 'My sexual orientation:']);
            $q->answers()->save(new Answer(['answer' => 'Heterosexual']));
            $q->answers()->save(new Answer(['answer' => 'Homosexual']));
            $q->answers()->save(new Answer(['answer' => 'Other']));
            
            $q = Question::create(['question' => 'Economic Background', 'category' => 'Demographic', 'checkbox' => true, 'question_client' => "I prefer my provider's income bracket to be...", 'question_therapist' => 'My economic bracket:']);
            $q->answers()->save(new Answer(['answer' => 'Low Income']));
            $q->answers()->save(new Answer(['answer' => 'Middle Class']));
            $q->answers()->save(new Answer(['answer' => 'High Income']));
            
            $q = Question::create(['question' => 'Marital Status', 'category' => 'Demographic', 'checkbox' => true, 'question_client' => "I prefer my provider's marital status to be...", 'question_therapist' => 'My marital status:']);
            $q->answers()->save(new Answer(['answer' => 'Married']));
            $q->answers()->save(new Answer(['answer' => 'Not married, living with mate']));
            $q->answers()->save(new Answer(['answer' => 'Was married, mate deceased']));
            $q->answers()->save(new Answer(['answer' => 'Separated or divorced']));
            $q->answers()->save(new Answer(['answer' => 'Single, never married']));
            
            $q = Question::create(['question' => 'Parental Status', 'category' => 'Demographic', 'checkbox' => true, 'question_client' => "I prefer my provider to have...", 'question_therapist' => 'I have:']);
            $q->answers()->save(new Answer(['answer' => 'No children']));
            $q->answers()->save(new Answer(['answer' => 'One child']));
            $q->answers()->save(new Answer(['answer' => 'Two children']));
            $q->answers()->save(new Answer(['answer' => 'Three or more children']));
            
            $q = Question::create(['question' => 'Religious Background', 'category' => 'Demographic', 'checkbox' => true, 'question_client' => "I prefer my provider's religious background to be...", 'question_therapist' => 'My religious background:']);
            $q->answers()->save(new Answer(['answer' => 'None']));
            $q->answers()->save(new Answer(['answer' => 'Jewish']));
            $q->answers()->save(new Answer(['answer' => 'Catholic']));
            $q->answers()->save(new Answer(['answer' => 'Buddhist']));
            $q->answers()->save(new Answer(['answer' => 'Protestant']));
            $q->answers()->save(new Answer(['answer' => 'Hindu']));
            $q->answers()->save(new Answer(['answer' => 'Muslim']));
            $q->answers()->save(new Answer(['answer' => 'Other established religion']));
            $q->answers()->save(new Answer(['answer' => 'Other spiritual practice']));

            $q = Question::create(['question' => 'Current Religious Practice', 'category' => 'Demographic', 'checkbox' => true, 'question_client' => "I prefer my provider's current religious practice to be...", 'question_therapist' => 'My current religious or spiritual practice:']);
            $q->answers()->save(new Answer(['answer' => 'None']));
            $q->answers()->save(new Answer(['answer' => 'Jewish']));
            $q->answers()->save(new Answer(['answer' => 'Catholic']));
            $q->answers()->save(new Answer(['answer' => 'Buddhist']));
            $q->answers()->save(new Answer(['answer' => 'Protestant']));
            $q->answers()->save(new Answer(['answer' => 'Hindu']));
            $q->answers()->save(new Answer(['answer' => 'Muslim']));
            $q->answers()->save(new Answer(['answer' => 'Other established religion']));
            $q->answers()->save(new Answer(['answer' => 'Other spiritual practice']));
            
            $q = Question::create(['question' => 'Importance of religion or spirituality', 'category' => 'Demographic', 'checkbox' => true, 'question_client' => "I prefer that my provider considers religion or spirituality to be...", 'question_therapist' => 'How important is religion or spirituality in my life?']);
            $q->answers()->save(new Answer(['answer' => 'Not important']));
            $q->answers()->save(new Answer(['answer' => 'Not very important']));
            $q->answers()->save(new Answer(['answer' => 'Somewhat important']));
            $q->answers()->save(new Answer(['answer' => 'Very Important']));
            $q->answers()->save(new Answer(['answer' => 'Extremely Important']));
            
            $q = Question::create(['question' => 'Level of religious or spiritual expertise', 'category' => 'Demographic', 'checkbox' => true, 'question_client' => "I prefer my provider's religious or spiritual expertise to be...", 'question_therapist' => 'My level of religious or spiritual expertise:']);
            $q->answers()->save(new Answer(['answer' => 'None']));
            $q->answers()->save(new Answer(['answer' => 'Some knowledge']));
            $q->answers()->save(new Answer(['answer' => 'Fairly advanced']));
            $q->answers()->save(new Answer(['answer' => 'Expert']));
    
            $q = Question::create(['question' => 'Position on medication', 'category' => 'Medication', 'checkbox' => true, 'question_client' => "I prefer my provider to hold the following view/s on medication...", 'question_therapist' => 'My stance on psychotropic medication:']);
            $q->answers()->save(new Answer(['answer' => 'Medications are usually useful, and should probably be tried for most clients with serious problems']));
            $q->answers()->save(new Answer(['answer' => 'Medications should be used very sparingly, and only tried in the most extreme cases, or after most other methods have been unsuccessful']));
            $q->answers()->save(new Answer(['answer' => 'Medications should never be used for psychological problems']));
    
            $q = Question::create(['question' => 'Importance of dream journaling', 'category' => 'Outlook', 'checkbox' => true, 'question_client' => "I prefer my provider to value dream journalling...", 'question_therapist' => 'I value dream journaling as a mode of healing:']);
            $q->answers()->save(new Answer(['answer' => 'Not at all']));
            $q->answers()->save(new Answer(['answer' => 'A little, the uses are limited']));
            $q->answers()->save(new Answer(['answer' => 'Somewhat, other methods are preferred']));
            $q->answers()->save(new Answer(['answer' => 'A lot, self work without dream journalling is folly']));
    
            $q = Question::create(['question' => 'Dream analysis expertise', 'category' => 'Outlook', 'checkbox' => true, 'question_client' => "I prefer my provider's expertise in dream analysis to be...", 'question_therapist' => 'My level of expertise in analysing dreams:']);
            $q->answers()->save(new Answer(['answer' => 'None']));
            $q->answers()->save(new Answer(['answer' => 'Some knowledge']));
            $q->answers()->save(new Answer(['answer' => 'Fairly advanced']));
            $q->answers()->save(new Answer(['answer' => 'Expert']));
            
            $q = Question::create(['question' => 'Professional License', 'category' => 'Outlook', 'checkbox' => true, 'question_client' => "I prefer my provider to have the following licensing...", 'question_therapist' => 'My level of professional licensing:']);
            $q->answers()->save(new Answer(['answer' => 'None']));
            $q->answers()->save(new Answer(['answer' => 'Some state granted licensing']));
            $q->answers()->save(new Answer(['answer' => 'Fully qualified by a relevant government authority']));
    
            $q = Question::create(['question' => 'Education Level', 'category' => 'Outlook', 'checkbox' => true, 'question_client' => "I prefer my provider to have the following level of academic education...", 'question_therapist' => 'My level of academic education:']);
            $q->answers()->save(new Answer(['answer' => 'None/Self taught']));
            $q->answers()->save(new Answer(['answer' => 'Some High-School education']));
            $q->answers()->save(new Answer(['answer' => 'High-School diploma']));
            $q->answers()->save(new Answer(['answer' => 'College or university degree']));
            $q->answers()->save(new Answer(['answer' => 'Masters or PHD level university degree']));
    
            $q = Question::create(['question' => 'Political Leaning', 'category' => 'Outlook', 'checkbox' => true, 'question_client' => "I prefer my provider to mostly identify politically as...", 'question_therapist' => 'I politically identify mostly with:']);
            $q->answers()->save(new Answer(['answer' => 'Anarcho-capitalist, the free market does not require a state']));
            $q->answers()->save(new Answer(['answer' => 'Conservative/minarchist, the state is a necessary evil, it must be minimized to basic roles']));
            $q->answers()->save(new Answer(['answer' => 'Liberal/socialist, the state is a means to an egalitarian end']));
            $q->answers()->save(new Answer(['answer' => 'Communist, private property must be abolished']));
    
            $q = Question::create(['question' => 'Role of Spanking', 'category' => 'Outlook', 'checkbox' => true, 'question_client' => "I prefer my provider to hold the following views around corporal punishment...", 'question_therapist' => 'My view on corporal punishment:']);
            $q->answers()->save(new Answer(['answer' => 'Spanking is child abuse, the initiation of violence is evil always and everywhere']));
            $q->answers()->save(new Answer(['answer' => 'Minor forms of corporal punishment, like spanking, while regrettable, can be an effective method of discipline']));
            $q->answers()->save(new Answer(['answer' => 'Corporal punishment is a necessary part of a displined and healthy childhood']));

            $q = Question::create(['question' => 'Objective Morality', 'category' => 'Outlook', 'checkbox' => true, 'question_client' => "I prefer my provider to hold the following views around morality...", 'question_therapist' => 'My view on morality:']);
            $q->answers()->save(new Answer(['answer' => 'Moral rules are subjective, whether something is good or bad is an opinion']));
            $q->answers()->save(new Answer(['answer' => 'Moral rules are objective, actions can be judged on universal standards']));
            $q->answers()->save(new Answer(['answer' => 'Moral rules are invalid']));
            
            $q = Question::create(['question' => 'Forgiveness', 'category' => 'Outlook', 'checkbox' => true, 'question_client' => "I prefer my provider to hold the following views around forgiveness...", 'question_therapist' => 'My view on forgiveness:']);
            $q->answers()->save(new Answer(['answer' => "Forgiving one's transgressors is a necessary part of healing"]));
            $q->answers()->save(new Answer(['answer' => "Forgiveness cannot be given without being earned but it is not owed even then"]));
            $q->answers()->save(new Answer(['answer' => "Forgiving one's transgressors is harmful to one's healing"]));

        }
        

    }

}
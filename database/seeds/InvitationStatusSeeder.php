<?php

use Illuminate\Database\Seeder;
use App\InvitationStatus;

class InvitationStatusSeeder extends Seeder {

    public function run()
    {
        DB::table('invitation_statuses')->delete();

        if (null == InvitationStatus::first())
        {
            InvitationStatus::create(['description' => 'Open']);
            InvitationStatus::create(['description' => 'Expired']);
            InvitationStatus::create(['description' => 'Accepted']);
            InvitationStatus::create(['description' => 'Closed']);
        }
    }

}
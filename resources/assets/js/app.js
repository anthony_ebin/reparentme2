$(function()
{
    var $burger = $("#burger");
    var $navbar = $("#navigation_menu");
    var $regForm = $(".register-form");
    var $collapseoffer = $(".collapse-offer");
    
    // hide certain fields on start
    hideStartFields();
    
    function hideStartFields()
    {
        $navbar.hide();
        $regForm.hide();
        $collapseoffer.hide();
    }
    
    $burger.click(function()
    {
        $navbar.toggle("slow");
    });
    
    // Cycle through quotes http://stackoverflow.com/questions/12065273/fade-in-out-text-loop-jquery
    var quotes = $(".quote");
    var quoteIndex = -1;

    function showNextQuote() 
    {
        ++quoteIndex;
        quotes.eq(quoteIndex % quotes.length)
            .fadeIn(2000)
            .delay(2000)
            .fadeOut(2000, showNextQuote);
    }

    showNextQuote(); 
    
    // show register form on main page when question is answered
    $("#main-page-question-submit").click(function()
    {
        $(this).parent().parent().hide("slow", function(){
            $regForm.show("slow");
        });
        
    }); 


    // if miniform answer is checked and importance is selected only then activate the button, else leave inactive
    
    function toggleNextButton(questionbox)
    {
        var $answersChecked = questionbox.find(".answer:checked").length;
        var $answerImportaneChecked = 1; //questionbox.find(".importance:checked").length;
    
        if ($answerImportaneChecked==0||$answersChecked==0) {
            questionbox.find(".toggle-question").addClass("inactive");
            questionbox.removeClass("answered");
        } else {
            questionbox.find(".toggle-question").removeClass("inactive");
            questionbox.addClass("answered");
        }
    }
    // on load run the toggle function
    toggleNextButton($(".mini-form"));
    // on click on form run the toggle function
    $(".answer").change(function() {
       toggleNextButton($(this).parents("fieldset"));
       // if all answers in questionnaire-question-options is checked then disable importance and default to none
       if($(this).parent().parent(".questionnaire-question-options").find(".answer:checked").length==$(this).parent().parent(".questionnaire-question-options").find(".answer").length)
       {
           $(this).parent().parent(".questionnaire-question-options").find(".importance").attr('disabled',true);
       } else $(this).parent().parent(".questionnaire-question-options").find(".importance").attr('disabled',false);
    });
    $(".importance").change(function() {
       toggleNextButton($(this).parents("fieldset")); 
    });
    
    
    // questionnaire animations
    
    //foreach question check answer and importance is checked
    // if yes then add class "answered" on question box
    $(".questionnaire-question").each( function( index, element ){
        toggleNextButton($(this)); 
    });
    
    // show next unanswered question
    function showNextUnansweredQuestion()
    {
        // hide all questions first
        $(".questionnaire-question-options").hide("slow");
        // show first unanswered
        $(".questionnaire-question:not(.answered)").first().children(".questionnaire-question-options").show("slow");
    }
    
    // hide all but first unanswered question at start
    showNextUnansweredQuestion();
    
    // on clicking of the answer button ajax update
    // then hide current question and show next unanswered question
    $(".toggle-question").click(function() {
        $.post('profile', $(this).parents("form").serialize(), function(response){
            showNextUnansweredQuestion();
        });
    });

    // toggle question show hide on click
    $(".questionnaire-question").click(function() {
        if ($(this).children(".questionnaire-question-options").is(":hidden")) {
            $(".questionnaire-question-options").hide("slow");
            $(this).children(".questionnaire-question-options").show("slow");
        }
    });
    
    // toggle offer on request page
    $("#collapse-offer-button").click(function() {
       $collapseoffer.toggle("slow");
       if ($(this).html()=="Show message") {
        $(this).html("Hide message");
       } else $(this).html("Show message");
    });
    
    // Filling question on main page goes to session and then in questionnaire view
    $(".mini-form .answer").click(function() {
        $sessionAnswers = $(".mini-form .answer:checked");
        
        $answers = [];
        $.each($sessionAnswers, function( index, answer ) {
            $questionID = answer.name;
            $answers.push(answer.value);
        });

        sessionStorage['sessionQuestionID'] = $questionID;
        sessionStorage['sessionAnswers'] = $answers;
    });
    
    // pull answer from session if any
    $selector = $(".questionnaire-question input[name='" + sessionStorage['sessionQuestionID'] + "']"); 
    // alert($($selector));
    if($($selector).prop('checked') == false)
    {
        arrayAnswer = sessionStorage['sessionAnswers'].split(',');
        // alert(arrayAnswer.constructor === Array);
        $.each(arrayAnswer, function(index, answer){
           $("input[value='"+answer+"']").prop('checked', true);
        });
    }
    
    // trigger the change event to enable the answer button
    $( ".answer" ).trigger("change");
    //https://jqueryui.com/datepicker/#min-max
    $( ".date" ).datepicker({ minDate: 1, maxDate: "+14D" });
    $( ".date" ).datepicker("option", "dateFormat","yy-mm-dd");
    
    //replace input box with multline textarea, doing it js because unit test will not work on textarea
    $text = $(".account_summary").val();
    $newtext = "<textarea name='account_summary' class='account_summary' placeholder='About my practice' cols='50' rows='10'>" + $text + "</textarea>";
    $(".account_summary").replaceWith($newtext);
    $text = $(".textarea-request").val();
    $newtext= "<textarea placeholder='Hi, I am looking for a therapist who can...' name='description' cols='50' rows='10'>" + $text + "</textarea>";
    $(".textarea-request").replaceWith($newtext);
    
    
        $("#ui-datepicker-div").hide();
});
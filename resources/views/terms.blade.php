@extends('layout')

@section('content')
<div class="content-container">
    
        <h2>TERMS AND CONDITIONS</h2>
    	<p>The terms and conditions may be revised at anytime.</p>
    	<p>This site is free to use.</p>
        <p>ReparentMe is a matching service only and is not responsible for any interactions and transactions between clients and providers.</p>
    	<p>By using this site you agree to be bound by these Terms of Use. If you wish to use this site, you must read, comply with and agree to be bound by the terms of this Agreement. If you object to anything in this Agreement, do not use this site. </p>
    	<p>Usage terms:</p>
    		<ul>
    			<li>Do not falsify information</li>
    			<li>Do not misuse the site in any way</li>
    			<li><b>Talk therapy only! No prescriptions, medication advice etc.</b></li>
    			<li>You are responsible to follow all laws and rules of the jurisdiction in which you operate</li>
    		</ul>

        <h5> Last updated on 19th July 2016 </h5>
</div>
@endsection
    @if (count($errors) > 0)
        <div class="error">
            <h2>There were issues with your submission</h2>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
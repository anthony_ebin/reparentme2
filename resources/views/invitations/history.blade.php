@extends('layout')

@section('content')

@if ($invites->count())
	<div class="header-title">
		<h2>Open Requests</h2>
		<p><strong>{{count($invites)}}</strong> request(s) listed</p>
	</div>

		<table class="requests-table">
			<thead>
				<tr>
					<th hidden>ID</th>
					<th>Request</th>
					<th class="hide-small">
						Deadline
					</th>
					<th class="hide-small">
						Budget range, USD per hour
					</th>
					<th class="hide-small">Created On</th>
					<th class="hide-small">Number of offers</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($invites as $invite)
					<tr>
						<td hidden>
							<a href="{{ url('/requests/'.$invite->id) }}">{{$invite->id}}</a>
						</td>
						<td> <a href="{{ url('/requests/'.$invite->id) }}">{{ Illuminate\Support\Str::words($invite->description,10) }} </a></td>
						<td class="hide-small"> {{ date('F d, Y', strtotime($invite->deadline)) }} </td>
						<td class="hide-small"> ${{ $invite->budget_range_min }} to ${{ $invite->budget_range_max }} </td>
						<td class="hide-small"> {{ date('F d, Y', strtotime($invite->created_at)) }} </td>
						<td class="hide-small"> {{ $invite->offers->count() }} </td>
					</tr>
				@endforeach
			</tbody>
		</table>
	@else
		You have made no requests
	@endif

	<?php echo $invites->render(); ?>


@endsection
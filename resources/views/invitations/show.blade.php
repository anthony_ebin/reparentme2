@extends('layout')

@section('content')
	
	<div class="show-request">
		<div class="header-title">
			<h2>{{ $is_owner_of_request?'My Request # ' . $invite->id:'Request # '.$invite->id }}</h2>
			<p>Posted on: <strong>{{ date('F d, Y', strtotime($invite->created_at)) }}</strong></p>
			<p>Ending on: <strong>{{ date('F d, Y', strtotime($invite->deadline)) }}</strong></p>
			<p>Budget range, per hour: <strong>USD ${{ $invite->budget_range_min }} to ${{ $invite->budget_range_max }}</strong></p>
		</div>
		
	
	    <div> <strong>Description:</strong> {!! $invite->description !!} </div>
	
		@if($is_owner_of_request && $invite->invitationStatus->description == 'Open')
			<form action="{{ url('/requests/'.$invite->id)}}" method="POST">
	            {!! csrf_field() !!}
	            {{ method_field('DELETE') }}
				<input type="submit" value="Close Request" class="btn btn-warning" onclick="return confirm('Are you sure you want to close this request?');"/>
			</form>
		@elseif ($invite->invitationStatus->description <> 'Open') <h5><b>Request is closed</b></h5>
		@endif

	</div>
	
	<div class="show-request">
		
		{{--Only approved providers can see the offer form--}}
	
		@if($is_provider&&!$is_owner_of_request&&!$existing_bid_for_this_therapist&&$invite->invitationStatus->description=='Open')
			<div class="offer-form-div">
				@include('common.errors')
				<h2>Make an offer to this request:</h2>
				<form action="{{url('/offers')}}" method="post">
		            {!! csrf_field() !!}
					<label for="hourly_price">Recommended hourly price, USD:</label>
					<input type="number" name="hourly_price" value="20" min="0" max="200" title="This is just a recommended price that you offer the client. It can be changed at any time."/>
					<label for="cover_letter">Cover letter:</label>
					<textarea name="cover_letter" cols="5" rows="10" placeholder="Describe your offer here..."></textarea>
		
		  			<input type="hidden" name="invitation_id" value="{{$invite->id}}">
		
					<input type="submit" value="Submit" class="btn btn-primary"/>
				</form>	
			</div>
		@endif
		
				{{--if I as a provider have already made a bid on this request then show me my bid--}}
		@if($existing_bid_for_this_therapist)
			<h2>My Offer</h2>
			<p>Rate offered: USD ${{ $existing_bid_for_this_therapist->hourly_price }} per hour</p>
			<p>{{ $existing_bid_for_this_therapist->cover_letter }}</p>
			{{--And if the request is closed then I cannot change the bid.--}}
			@if($invite->invitationStatus->description=='Open')
				<form action="{{ url('/offers/'.$existing_bid_for_this_therapist->id) }}" method="POST">
		            {!! csrf_field() !!}
		            {{ method_field('DELETE') }}
					<input type="submit" value="Withdraw Offer" class="btn btn-warning" onclick="return confirm('Are you sure you want to withdraw this offer?');"/>
				</form>
			@endif
		@endif

		<!--Owners can see bids-->
		@if($existing_bids_for_this_request&&$is_owner_of_request)
			<h2>Offers</h2>
			@foreach($existing_bids_for_this_request as $existing_bid_for_this_request)
				<hr>
				<div class="offer-row">
					<a href="{{ url('/provider', $existing_bid_for_this_request->therapist->id. "-". str_slug($existing_bid_for_this_request->therapist->user->name)) }}"
					   title={{ $existing_bid_for_this_request->therapist->user->name }} 
					   class="thumbnail">
				   	   <img src="{{ Gravatar::src($existing_bid_for_this_request->therapist->user->email, 60) }}" 
				   			width=60 
				   			alt={{ $existing_bid_for_this_request->therapist->user->name }} />
	   				</a>
					<div class="offer-row-details">
						<div>Provider: <strong>{{ $existing_bid_for_this_request->therapist->user->name }}</strong></div>
						<div>Offer price: <strong>USD ${{ $existing_bid_for_this_request->hourly_price }}</strong></div>
					</div>
					<a class="btn btn-primary" id="collapse-offer-button">Show message</a>
				</div>
				<div class="collapse-offer">Offer: <strong>{{ $existing_bid_for_this_request->cover_letter }}</strong></div>
			@endforeach
		@endif
	</div>
	
		
@endsection
@include('common.errors')
<form action="{{ url('requests') }}" method="POST">
    {!! csrf_field() !!}
    <input class="textarea-request" type="text" name="description" value="{{isset($anonRequest) ? $anonRequest[0] : null}}" placeholder="Hi, I'm looking for a therapist who can..."/>
    <label for="deadline">I need an answer by:</label>
    <input type="text" name="deadline" value="{{isset($anonRequest) ? $anonRequest[1] : date('Y-m-d', strtotime('+1 weeks'))}}" class="date"/>
    <label for="budget_range_min">I can afford at least (USD per hour):</label>
    <input type="number" name="budget_range_min" value="{{ isset($anonRequest) ? $anonRequest[2] : 20 }}" min="0" max="200"/>
    <label for="budget_range_max">and at most (USD per hour):</label>
    <input type="number" name="budget_range_max" value="{{ isset($anonRequest) ? $anonRequest[3] : 60 }}" min="0" max="200"/>
    <input type="submit" value="{{isset($submitButtonText)?$submitButtonText:'Submit'}}" class="btn btn-primary"/>
</form>
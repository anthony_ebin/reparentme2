@extends('layout')

@section('content')

<div id="home_section_anon_req"> <!--Start of anon request on home page -->
        <h1>Post an anonymous request, let our providers approach you</h1>
        
        <div class="info-box-container">   
            <div class="info-box">
                <ul>
                    <li><i class="material-icons">keyboard_arrow_right</i>Not sure how to find a therapist?</li>
                    <li><i class="material-icons">keyboard_arrow_right</i>Post an anonymous and brief summary of what you are looking for</li>
                    <li><i class="material-icons">keyboard_arrow_right</i>Let providers approach you with their offers</li>
                    <li><i class="material-icons">keyboard_arrow_right</i>Choose one that best suits you</li>
                    <div class="sub-info">
                        <h4>Is this secure?</h4>
                        <p>This request will be totally anonymous and confidential, none of your personal information will be shared <b>(Please do not include your personal info in the free text).</b> Providers will only see the details you provide in the fields.</p>
                    </div>
                </ul>
            </div>
            
            <div class="anon-req-form"><!--Anon request form partial -->
                @include('invitations.form', ['submitButtonText' => 'Submit'])
            </div>
        </div>

</div> <!--End of anon request on home page -->

@endsection
@extends('layout')

@section('content')
<div class="content-container">
    <h2>About ReparentMe</h2>
    <p>ReparentMe is a <b>100% free</b> service for <b>professional</b> or <b>freelance</b> therapists, counsellors, advisors and life coaches to represent their practice online and get matched with clients using our proprietary algorithm.</p>

	<ul>
		<li><b>No commissions or fees</b></li>
		<li>Provide online (skype/facetime etc.) talk therapy to clients all over the world</li>
		<li>No need to worry about finding clients anymore, let the clients approach you based on your profile</li>
		<li>Whether you are just starting up or have been providing advisory services for decades, ReparentMe allows you to transition to the new online economy at <b>zero cost</b></li>
		<li>ReparentMe is a matching service only, we <b>do not interfere or participate</b> in any client-provider interactions or transactions. All interactions are directly between the client and the provider and the provider is responsible to follow all the rules and regulations as per their jurisdiction.</li>
	</ul>

	<p>There are thousands of clients looking for online counselling and advisory services. Get found and build your online practice without the uncertainty of building a private practice from scratch or the high fees of joining a corporate practice</p>

	<p>Join now by visiting our sign up page <b><a href="{{ url('registerprovider') }}">here</a></b></p>

    <div class="separator"></div>
    
    <small>
		<p>This is a personal project by Anthony Ebin</p>
		<p>Feel free to get in touch with me via:</p>
		<ul>
			<li>Email: admin@reparent.me</li>
		</ul>
	</small>
</div>
@endsection
@extends('layout')

@section('content')
<div class="providers-container">

    <div class="provider-row">
        <div class="provider-gravatar">
            <a href="{{ url('/profile') }}" title="{{ $client->name }}" class="thumbnail"><img src="{{ Gravatar::src($client->email, 250) }}" alt="{{ $client->name }}"/></a>
        </div>
        <div class="provider-row-details">
            <div class="provider-row-name">
                <a href="{{ url('/profile') }}">{{ $client->name }}</a>
                @if (isset($user_active_request_id))
                <a href={!! url('requests/'.$user_active_request_id)  !!}>My Active Request</a>
                @else <p>No <b>Active</b> Requests</p>
                @endif
                <a href={!! url('requests/?my_request=1')  !!}>Request History</a>
                @if(Auth::User()->hasRole('therapist'))
                <a href={!! url('offers')  !!}>My Bids</a>
                <a href={!! url('providerprofile')  !!}>My Provider Profile</a>
                <a href={!! url('provider/' . $client->id)  !!}>Public View</a>
                @endif
                <p>Want to change your profile photo? We pull from <a href="http://gravatar.com">gravatar.com</a>.</p>
            </div>
        </div>
    </div>
    
    
    <div class="provider-about" id="about-me">
        <h2 id="my-profile">My Client Profile</h2>
        <p>Answer as many questions as you want to get matched with our providers</p>
            <form action="{{ url('profile') }}" method="POST" class="questionnaire">
            {!! csrf_field() !!}
        @foreach ($questions as $question)
            <fieldset class="questionnaire-question">
                <div class="legend">{{ $question->question_client }}</div>
                <div class="questionnaire-question-options">
                    @foreach ($question->answers()->get() as $answer)
                        @if( isset($client) && ($client->answers()->where('answer_id', $answer->id)->first()))
                            <div class="answer_segment">
                                <input type="{{ $question->checkbox==true?'checkbox' : 'radio' }}" checked
                                name="{{ $question->checkbox==true?$answer->question_id.'[]' : $answer->question_id }}" 
                                value="{{ $answer->id }}"
                                class="answer">
                                <label for="{{ $question->checkbox==true?$answer->question_id.'[]' : $answer->question_id }}">{{ $answer->answer }}</label>
                            </div>
                        @else
                            <div class="answer_segment">
                                <input type="{{ $question->checkbox==true?'checkbox' : 'radio' }}" 
                                name="{{ $question->checkbox==true?$answer->question_id.'[]' : $answer->question_id }}" 
                                value="{{ $answer->id }}"
                                class="answer">
                                <label for="{{ $question->checkbox==true?$answer->question_id.'[]' : $answer->question_id }}">{{ $answer->answer }}</label>
                            </div>
                        @endif
                    @endforeach
                    
                    <div class="answer_importance">
                        <div class="legend">How important is this question to you?</div>
                        @if( isset($client) && count($client->answers()->where('question_id',$question->id)->first()))
                            @if($client->answers()->where('question_id',$question->id)->first()->pivot->importance == 0)
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="none" class="importance" checked>Not at all
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="little" class="importance">A little important
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="somewhat" class="importance">Somewhat important
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="very" class="importance">Very important
                            @elseif($client->answers()->where('question_id',$question->id)->first()->pivot->importance == 1)
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="none" class="importance">Not at all
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="little" class="importance" checked>A little important
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="somewhat" class="importance">Somewhat important
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="very" class="importance">Very important
                            @elseif($client->answers()->where('question_id',$question->id)->first()->pivot->importance == 10)
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="none" class="importance">Not at all
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="little" class="importance">A little important
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="somewhat" class="importance" checked>Somewhat important
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="very" class="importance">Very important
                            @elseif($client->answers()->where('question_id',$question->id)->first()->pivot->importance == 50)
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="none" class="importance">Not at all
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="little" class="importance">A little important
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="somewhat" class="importance">Somewhat important
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="very" class="importance" checked>Very important
                            @else
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="none" class="importance">Not at all
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="little" class="importance">A little important
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="somewhat" class="importance">Somewhat important
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="very" class="importance">Very important
                            @endif
                        @else
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="none" class="importance" checked>Not at all
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="little" class="importance">A little important
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="somewhat" class="importance">Somewhat important
                                <input type="radio" name="{{ $question->id . '_importance' }}" value="very" class="importance">Very important
                        @endif
                    </div>
                    <input type="button" class="btn btn-primary toggle-question" value="Answer"/>
                </div>
            </fieldset>
        @endforeach
    
            <input type="submit" value="Submit" hidden/>
        </form>
    

    </div>
    
</div>
@endsection
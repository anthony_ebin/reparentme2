@extends('layout')

@section('content')

	<div class="header-title">
		<h2>My offers</h2>
		<p><strong>{{count($offers)}}</strong> offer(s) listed</p>
	</div>
	
	@if ($offers->count())

		<table class="offers-table">
			<thead>
			<tr>
				<th>ID</th>
				<th>Cover Letter</th>
				<th>Offered Hourly Price, USD</th>
				<th>Offer Date</th>
				<th>Request Link</th>
				<th>Request Status</th>
			</tr>
			</thead>
			<tbody>

				@foreach ($offers as $offer)
					<tr>
						<td><a href="{{ url('/offers/'.$offer->id) }}">{{$offer->id}}</a></td>
						<td> {{ Illuminate\Support\Str::words($offer->cover_letter,10) }}...</td>
						<td> {{ $offer->hourly_price }}</td>
						<td> {{ date('F d, Y', strtotime($offer->created_at)) }} </td>
						<td><a href="{{ url('/requests/'.$offer->invitation->id) }}">See Request</a></td>
{{--						<td> {{ date('F d, Y', strtotime($offer->invitation->deadline)) }} </td>--}}
						<td> {{ $offer->invitation->invitationStatus->description }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	@endif

<?php echo $offers->render(); ?>

@endsection
@extends('layout')

@section('content')

	<div class="show-offer-box">
		@if($offer->invitation->invitationStatus()->first()->description <> 'Open')
			<h5> The request for this offer is no longer active </h5>
		@endif
		<a href="{{ url('/requests/'.$offer->invitation->id) }}">Link to request</a>
		<h5> Hourly Price Offered: USD ${{ $offer->hourly_price }}</h5>
		<h5> Cover Letter: {{ $offer->cover_letter }}</h5>
	</div>
@endsection
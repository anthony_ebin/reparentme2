@extends('layout')

@section('content')
<div class="banner">
    <div class="banner-box">
        <h2>Register</h2>
        @include('auth.registerpartial')
    </div>
</div>
@endsection

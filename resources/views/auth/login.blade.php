@extends('layout')

@section('content')
<div class="banner">
    <div class="banner-box">
        <h2>Login</h2>
        <form method="POST" action="{{ url('/login') }}">
            {!! csrf_field() !!}
            
            <input type="email" name="email" value="{{ old('email') }}" placeholder="email">
            @if ($errors->has('email'))
                <strong>{{ $errors->first('email') }}</strong>
            @endif
            
            <input type="password" name="password" placeholder="password">
            @if ($errors->has('password'))
                <strong>{{ $errors->first('password') }}</strong>
            @endif
            
            <label>
                <input type="checkbox" name="remember"> Remember Me
            </label>
    
            <button type="submit" class="btn btn-primary">
            Login
            </button>
    
            <a href="{{ url('/password/reset') }}">Forgot Your Password?</a>
        </form>
    </div>
</div>
@endsection

<form method="POST" action="{{ url('/register') }}">
    {!! csrf_field() !!}

    <input type="text" name="name" value="{{ old('name') }}" placeholder="Name">

    @if ($errors->has('name'))
        <strong>{{ $errors->first('name') }}</strong>
    @endif

    <input type="email" name="email" value="{{ old('email') }}" placeholder="E-Mail Address">

    @if ($errors->has('email'))
        <strong>{{ $errors->first('email') }}</strong>
    @endif

    <input type="password" name="password" placeholder="Password">

    @if ($errors->has('password'))
        <strong>{{ $errors->first('password') }}</strong>
    @endif

    <input type="password" name="password_confirmation" placeholder="Confirm Password">

    @if ($errors->has('password_confirmation'))
        <strong>{{ $errors->first('password_confirmation') }}</strong>
    @endif

    <button type="submit" class="btn btn-primary">
        {{ isset($submitButtonText)?$submitButtonText:'Register' }}
    </button>
</form>
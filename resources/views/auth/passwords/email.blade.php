@extends('layout')

<!-- Main Content -->
@section('content')
<div class="banner">
    <div class="banner-box">
        <h2>Reset Password</h2>
        @if (session('status'))
            {{ session('status') }}
        @endif
        
        <form method="POST" action="{{ url('/password/email') }}">
            {!! csrf_field() !!}
        
            <label for="email">E-Mail Address</label>
        
            <input type="email" name="email" value="{{ old('email') }}">
        
            @if ($errors->has('email'))
                <strong>{{ $errors->first('email') }}</strong>
            @endif
        
            <button type="submit" class="btn btn-primary">
                Send Password Reset Link
            </button>
        </form>
    </div>
</div>
@endsection

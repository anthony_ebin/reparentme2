@extends('layout')

@section('content')
<div class="banner">
    <div class="banner-box">
        <h2>Reset Password</h2>

        <form method="POST" action="{{ url('/password/reset') }}">
            {!! csrf_field() !!}
        
            <input type="hidden" name="token" value="{{ $token }}">
        
            <label>E-Mail Address</label>
            <input type="email" name="email" value="{{ $email or old('email') }}">
        
            @if ($errors->has('email'))
                <strong>{{ $errors->first('email') }}</strong>
            @endif
            
            <label>Password</label>
            <input type="password" name="password">
        
            @if ($errors->has('password'))
                <strong>{{ $errors->first('password') }}</strong>
            @endif
        
            <label>Confirm Password</label>
            <input type="password" class="form-control" name="password_confirmation">
        
            @if ($errors->has('password_confirmation'))
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            @endif
        
            <button type="submit" class="btn btn-primary">
                Reset Password
            </button>
        </form>
        
    </div>
</div>
@endsection

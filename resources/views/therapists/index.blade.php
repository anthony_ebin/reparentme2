@extends('layout')

@section('content')

    <div class="search-bar">
        <form action="{{url('provider')}}" method="get">
            <label for="budget">I'm looking for an hourly rate around...</label>
            <input min="0" max="200" name="budget" value="30" type="number">
            <input type="submit" value="Submit" class="btn btn-primary"/>
        </form>
    </div>

    <div class="result-count">
        <strong>{{count($therapists)}}</strong> provider(s) listed
    </div>
    <div class="sort-options">
        <label for="budget">Sort by...</label>
        {!! Auth::check()?"<a href='?sortby=best'>Best match</a>":''!!}
        <a href="?sortby=highprice">Highest Price</a>
        <a href="?sortby=lowprice">Lowest Price</a>
    </div>

    <div class="providers-container">
    @foreach($therapists as $therapist)
        <div class="provider-row">
            <div class="provider-gravatar">
                <a href="{{ url('/provider', $therapist->user_id. "-". str_slug($therapist->name)) }}" title="{{ $therapist->name }}" class="thumbnail"><img src="{{ Gravatar::src($therapist->email, 250) }}" alt="{{ $therapist->name }}"/></a>
            </div>
            <div class="provider-row-details">
                <div class="provider-row-name">
                    <h2><a href="{{ url('/provider', $therapist->user_id. "-". str_slug($therapist->name)) }}">{{ $therapist->name }}</a></h2>
                    <h3 class="score">{{ $totalscore==0?'': sprintf("%.2f%%", ($therapist->score * 100)).' match'  }}</h3>
                    <p>{{ Illuminate\Support\Str::words($therapist->account_summary,30) }}</p>
                </div>
                <div class="provider-row-details-sub">
                    <ul>
                        <li><b>Price range:</b> ${{ $therapist->standard_hourly_rate_min }} to ${{ $therapist->standard_hourly_rate_max }} USD per hour</li>
                    @if ($therapist->first_session_rate == 0)
                        <li><b>First session FREE</b></li>
                    @else
                        <li><b>First session rate:</b> ${{ $therapist->first_session_rate }} USD</li>
                    @endif
                        <li><b>First session length:</b> {{ $therapist->first_session_length }} Minutes</li>
                    </ul>
                </div>
            </div>
        </div>
    @endforeach
    </div>
    

@endsection
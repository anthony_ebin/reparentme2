@extends('layout')

@section('content')
<div class="providers-container">
    
    <div class="provider-row">
        <div class="provider-gravatar">
            <a href="{{ url('/provider', $therapist->id. "-". str_slug($therapist->name)) }}" title="{{ $therapist->name }}" class="thumbnail"><img src="{{ Gravatar::src($therapist->email, 250) }}" alt="{{ $therapist->name }}"/></a>
        </div>
        <div class="provider-row-details">
            <div class="provider-row-name">
                <h2><a href="{{ url('/provider', $therapist->id. "-". str_slug($therapist->name)) }}">{{ $therapist->name }}</a></h2>
                <div class="provider-links">
                    <div class="provider-detail-link"><a href="#about-me">About Me</a></div>
                    <div class="provider-detail-link"><a href="#my-profile">My Profile</a></div>
                    <div class="provider-detail-link"><a href="#contact">Contact Me</a></div>
                </div>
            </div>
            <div class="provider-row-details-sub">
                <ul>
                    <li><b>Price range:</b> ${{ $therapist->therapist->standard_hourly_rate_min }} to ${{ $therapist->therapist->standard_hourly_rate_max }} USD per hour</li>
                @if ($therapist->therapist->first_session_rate == 0)
                    <li><b>First session FREE</b></li>
                @else
                    <li><b>First session rate:</b> ${{ $therapist->therapist->first_session_rate }} USD</li>
                @endif
                    <li><b>First session length:</b> {{ $therapist->therapist->first_session_length }} Minutes</li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="provider-about" id="about-me">
        <h2>About Me</h2>
        <p>{{$therapist->therapist->account_summary }}</p>
        
        <h2 id="my-profile">My Profile</h2>
        @foreach ($questions as $question)
        <fieldset>
            <div class="profile-question">
                <strong>{{ $question->question }}:</strong>
                @foreach ($question->answers()->get() as $answer)
                    @if(count($therapist->therapist->answers()->where('answer_id', $answer->id)->first())>0)
                        {{ $answer->answer }}
                    @endif
                @endforeach
            </div>
        </fieldset>
        @endforeach

    </div>
    
    <div class="contact-form" id="contact">
        <h2>Contact Me</h2>
        <form action="{{ url('provider/'. $therapist->id .'/message') }}" method="POST">
            {!! csrf_field() !!}
            <textarea name="message_body" placeholder="Dear {{ $therapist->name }}, ..." cols="5" rows="10"></textarea>
            <div class="from-field">
                <label for="sender_email">From:</label>
                <input type="email" name="sender_email" value="{{ null!==(Auth::user())?Auth::user()->email:'' }}" placeholder="abc@email.com"/>
                <input type="text" name="provider_id" hidden value="{{$therapist->id}}"/>
            </div>
            <input type="submit" value="Send" class="btn btn-primary"/>
        </form>
    </div>
    
</div>
@endsection
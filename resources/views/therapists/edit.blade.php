@extends('layout')

@section('content')


<div class="providers-container">
    <div class="provider-about">
        <h2>My Provider Profile</h2>
        @include('common.errors')
        
        <form class="provider-profile-questionnaire" action="{{ url('provider') }}" method="POST">
            {!! csrf_field() !!}            
            <label for="terms">ReparentMe is a matching service only and is not responsible for any interactions and transactions between clients and providers. 
                                By checking the adjacent box, you agree to abide by these terms and all policies listed and updated <a href="{{ url('terms') }}">here</a>.</label>
            @if(isset($therapist))
                <input type="checkbox" name="terms" checked/>
            @else
                <input type="checkbox" name="terms"/>
            @endif
            <label for="account_summary">Account Summary:</label>
            <input type="text" name="account_summary" class="account_summary" placeholder="About my practice" value="{{ isset($therapist)?$therapist->account_summary:'' }}"/>
            <label for="first_session_rate">First Session Rate, in USD:</label>
            <input type="number" name="first_session_rate" min="0" max="300" placeholder="First session rate in USD" value="{{ isset($therapist)?$therapist->first_session_rate:'' }}"/>
            <label for="first_session_length">First Session Lenght, in minutes:</label>
            <input type="number" name="first_session_length" min="0" max="120" placeholder="First session length in minutes" value="{{ isset($therapist)?$therapist->first_session_length:'' }}"/>
            <label for="standard_hourly_rate_min">Minimum Hourly Rate, in USD:</label>
            <input type="number" name="standard_hourly_rate_min" min="0" max="300" placeholder="Minimum hourly rate in USD" value="{{ isset($therapist)?$therapist->standard_hourly_rate_min:'' }}"/>
            <label for="standard_hourly_rate_max">Maximum Hourly Rate, in USD:</label>
            <input type="number" name="standard_hourly_rate_max" min="0" max="300" placeholder="Maximum hourly rate in USD" value="{{ isset($therapist)?$therapist->standard_hourly_rate_max:'' }}"/>

        
        <p><b>The following questionnaire is optional,</b> but we recommend that you answer as many questions as you can to allow more clients to be matched with you.</p>
        <input id="end-submit" type="submit" value="Skip and Submit" class="btn btn-primary"/>
        <div class="separator"></div>
        @foreach ($questions as $question)
            <fieldset class="questionnaire-question">
                <div class="legend">{{ $question->question_therapist }}</div>
                @foreach ($question->answers()->get() as $answer)
                    @if( isset($therapist) && count($therapist->answers()->where('answer_id', $answer->id)->first())>0)
                        <input type="radio" name="{{ $answer->question_id }}" value="{{ $answer->id }}" checked>{{ $answer->answer }}</br>
                    @else
                        <input type="radio" name="{{ $answer->question_id }}" value="{{ $answer->id }}" >{{ $answer->answer }}</br>
                    @endif
                @endforeach            
            </fieldset>
        @endforeach
    
            <input id="end-submit" type="submit" value="Submit" class="btn btn-primary"/>
        </form>
    </div><!--end provider-about-->
</div> <!--end providers-container-->
@endsection
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Find therapists, counselors and advisers offering online therapy and counselling on Skype or Google Hangout">
        <meta name="keywords" content="therapy, counseling, counselling, online therapy, skype therapy, online counselling, cheap online therapy, free online therapy">
        <title>Reparent Me | The online marketplace for emotional health</title>
        <!-- CSS And jQuerys -->
        <!--Icons https://design.google.com/icons/-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--Font-->
        <link href='https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans:400,300,800' rel='stylesheet' type='text/css'>
        <script src="https://code.jquery.com/jquery-3.0.0.min.js" integrity="sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0=" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
        <!--my styles-->
        <link rel="stylesheet" href="/css/app.css">
        <!--my scripts-->
        <script type="text/javascript" src="/js/all.js"></script>
        <!--Analytics-->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        
          ga('create', 'UA-67487151-1', 'auto');
          ga('send', 'pageview');
        </script>

    </head>
    
    <body>
        <div id="container">

            <nav> <!--Start of nav -->
                <div id="logo"><a href="/">Reparent<b>Me</b></a></div>
                <div id="navigation_menu">
                    <a href="{{ url('provider') }}">Match Providers</a>
                    <a href="{{ url('requests/create') }}">Anonymous Request</a>
                            
                    @if(null !== Auth::user())
                        @if(Auth::user()->hasRole('therapist'))
                        <a href="{{ url('requests') }}">Find Clients</a>
                        <a href="{{ url('providerprofile') }}">Provider Profile</a>
                        @else
                        <a href="{{ url('registerprovider') }}">Become a provider</a>
                        @endif
                    <a href="{{ url('profile') }}">Dashboard</a>
                    <a href="{{ url('logout') }}">Log Out</a>
                    @else
                    <a href="{{ url('login') }}">Log In</a>
                    <a href="{{ url('register') }}">Register</a>
                    @endif
                </div>
                                
                <i class="material-icons" id="burger">menu</i>
                
            </nav> <!--End of nav -->
            
            @yield('content')
            
           <footer>
               <div><a href="{{ url('about') }}">About</a></div>
               <div>&copy; Copyright 2016</div>
               <div>Image Credit: Alex Miguel Livia Jaen</div>
           </footer>
       </div> 

    </body>
    
</html>
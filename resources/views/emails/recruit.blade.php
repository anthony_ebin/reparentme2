<div>
    <p>Hello,</p>

    <p>
        I run a project that connects counsellors with clients on the internet. 
        Based on your profile I have shortlisted you as a potential provider for the first phase of the project. 
        As an early joiner you have the benefit of not paying any fees or commissions!
    </p>

    <p>
        Here’s a little about my project and if you are interested, the link to join is at the bottom:
    </p>
    
    <p>
        ReparentMe is a <b>100% free</b> service for <b>professional</b> or <b>freelance</b> therapists, counsellors,
        advisors and life coaches to represent their practice online and get matched with clients using our proprietary algorithm.
    </p>
    
    <ul>
        <li>No commissions or fees</li>
        <li>Provide online (skype/facetime etc.) talk therapy to clients all over the world</li>
        <li>No need to worry about finding clients anymore, let the clients approach you based on your profile</li>
        <li>Whether you are just starting up or have been providing advisory services for decades, ReparentMe allows you to transition to the new online economy at zero cost</li>
        <li>ReparentMe is a matching service only, we do not interfere or participate in any client-provider interactions or transactions. All interactions are directly between the client and the provider and the provider is responsible to follow all the rules and regulations as per their jurisdiction.</li>
    </ul>
    
    <p>
        There are thousands of clients looking for online counselling and advisory services. 
        Get found and build your online practice without the uncertainty of building a private practice from scratch or the high fees of joining a corporate practice.
    </p>
    
    <p>
        Join now by visiting our sign up page here: <a href="https://reparent.me/registerprovider">https://reparent.me/registerprovider</a>
    </p>
    
    <p>If you have any questions, feel free to reach out to me.</p>
    
    <p>
        Best regards,<br>
        Anthony E<br>
        <a href="mailto:anthony@reparent.me">anthony@reparent.me</a>
    </p>
</div>
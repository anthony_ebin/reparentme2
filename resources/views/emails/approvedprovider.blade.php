<p>Hello {{$user->name}},</p>

<p>You have signed up as a provider on ReparentMe!</p>

<p>======================================</p>

<p>ReparentMe is not responsible for any agreements not explicitly defined on our site.</p> 
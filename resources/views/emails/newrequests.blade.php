<div>
    <h5>Dear {{ $user->name }},</h5>

    <p>There were new requests made today. Please follow this link to find more information:</p>

    <h5><a href={{ url('requests')}}>See new requests</a></h5>
</div>
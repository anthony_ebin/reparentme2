<div>
    <h5>Hello,</h5>

    <p>You have received new offers today for your anonymous request. Please follow this link to find more information:</p>

    <h5><a href={{ url('requests')."/".$id }}>See offers for my request</a></h5>
</div>
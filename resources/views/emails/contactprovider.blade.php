<p>Hello {{$user->name}},</p>

<p>Please see below for a message from: {{ $sender }}</p>

<p>{{ $body }}</p>


<p>======================================</p>

<p>You may contact the client directly at the email address provided.</p>
<p>ReparentMe is not responsible for any agreements not explicitly defined on our site.</p>
@extends('layout')

@section('content')

<div id="home-banner" class="banner"> <!--Start of home banner -->
    <h1 class="quote">The online marketplace for emotional health</h1>
    <h1 class="quote">"The OKCupid of finding a healer"</h1>
    <h2>Find the right counsellor, advisor or therapist for your emotional needs</h2>
    
    <div class="info-box-container">
        <div class="info-box">
            <ul>
                <li><i class="material-icons">keyboard_arrow_right</i>100% free service</li>
                <li><i class="material-icons">keyboard_arrow_right</i>Fill a short questionnaire</li>
                <li><i class="material-icons">keyboard_arrow_right</i>Our matching algorithm helps you find the perfect advisor</li>
            </ul>
        </div>
        <div class="mini-form">
            <fieldset>
                <legend><b>{{ $question->question_client }}</b></legend>
            @foreach ($answers as $answer)
                <div class="answer_segment">
                    <input type="{{ $question->checkbox==true?'checkbox' : 'radio' }}" 
                           name="{{ $question->checkbox==true?$answer->question_id.'[]' : $answer->question_id }}" 
                           value="{{ $answer->id }}"
                           class="answer">
                    <label for="{{ $question->checkbox==true?$answer->question_id.'[]' : $answer->question_id }}">{{ $answer->answer }}</label>
                </div>
            @endforeach
                <input type="button" class="btn btn-primary toggle-question" id="main-page-question-submit" value="Next">
            </fieldset>
        </div>
        <div class="register-form">
            @include('auth.registerpartial', ['submitButtonText' => 'Register to complete the matching process'])
        </div>
    </div>
</div>  <!--End of home banner -->


<div id="home_section_anon_req"> <!--Start of anon request on home page -->
        <h1>...Or, post an anonymous request, let our providers approach you</h1>
        
        <div class="info-box-container">   
            <div class="info-box">
                <ul>
                    <li><i class="material-icons">keyboard_arrow_right</i>Not sure how to find a therapist?</li>
                    <li><i class="material-icons">keyboard_arrow_right</i>Post an anonymous and brief summary of what you are looking for</li>
                    <li><i class="material-icons">keyboard_arrow_right</i>Let providers approach you with their offers</li>
                    <li><i class="material-icons">keyboard_arrow_right</i>Choose one that best suits you</li>
                    <div class="sub-info">
                        <h4>Is this secure?</h4>
                        <p>This request will be totally anonymous and confidential, none of your personal information will be shared <b>(Please do not include your personal info in the free text).</b> Providers will only see the details you provide in the fields.</p>
                    </div>
                </ul>
            </div>
            
            <div class="anon-req-form"><!--Anon request form partial -->
                @include('invitations.form', ['submitButtonText' => 'Register and submit'])
            </div>
        </div>

</div> <!--End of anon request on home page -->

<div id="home_section_join"> <!--Start of join as provider on home page -->
       <h1>Join Us!</h1>
       <div class="info-box">
            <ul>
                <li><i class="material-icons">keyboard_arrow_right</i>Are you a therapist, counsellor or advisor?</li>
                <li><i class="material-icons">keyboard_arrow_right</i>Get matched with hundreds of clients using our proprietary algorithm</li>
                <li><i class="material-icons">keyboard_arrow_right</i>Join us, it's 100% FREE</li>
                <a href="#" class="btn btn-primary">Become a provider!</a>
            </ul>
       </div>
</div> <!--End of join as provider on home page -->
 



@endsection

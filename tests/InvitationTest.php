<?php

use App\Invitation;

class InvitationTest extends TestCase
{
    /**
     * User makes an anonymous request for a therapy session
     *
     * @return void
     */
    public function testAnonRequest()
    {
        $this->registerDummy('testClient', 'client');
        
        $this->makeInvitation('Random request made up to test anonymous request from client to therapist.');
        
        // Then My request is published 'This is my description to request for therapy. Please help me yo!'
        $this->asserttrue(count(Invitation::whereDescription('Random request made up to test anonymous request from client to therapist.')->get())==1);
        
        
        // dd(Invitation::isAlive()->with('offers')->latest('created_at')->first());
        // check if request appears in requests view from provider perspective
        // user can only see his request not others
        $this->visit('requests')
             ->dontSee('Random request made up to test anonymous request')
             ->seePageIs('profile');

        $this->visit('logout');
        
        $this->registerDummy('testTherapist', 'therapist');
        $this->visit('requests')
             ->see('Random request made up to test anonymous request');

    }


    /**
     * Client can only have one active public request at a time
     *
     * @return void
     */
    public function testSinglyActiveRequest()
    {
        $this->registerDummy('testClient', 'client');
        
        $this->makeInvitation('This is my description to request for therapy. Please help me yo.');
    
        $savedInvite = Invitation::whereDescription('This is my description to request for therapy. Please help me yo.')->get();
        $this->asserttrue(count($savedInvite)==1);
        
        $this->visit('requests/create')
             ->seePageIs('requests/'.$savedInvite->first()->id);
    }

    /**
     * Guests make anon request and should see existing answers when registered
     *
     * @return void
     */
    public function testGuestRequest()
    {        
        $this->makeInvitation('This is my description to request for therapy before login. Please help me yo.');
        
        $this->registerDummy('testClient', 'client');
        
        $this->visit('requests/create')
             ->see('This is my description to request for therapy before login. Please help me yo.')
             ->press('Submit');
             
        $this->asserttrue(count(Invitation::whereDescription('This is my description to request for therapy before login. Please help me yo.')->get())==1);
    }
    
    /**
     * Providers bid on active public requests
     *
     * @return void
     */
    public function testBidding()
    {
        $this->registerDummy('testClient', 'client');
        
        $this->makeInvitation('This is my description to request for therapy. Please help me yo.');
        
        $this->visit('logout');
        
        $this->registerDummy('testProvider1', 'therapist');
        
        $text = substr(strip_tags('This is my description to request for therapy. Please help me yo.'),0,50);
        $inviteid = Invitation::whereDescription('This is my description to request for therapy. Please help me yo.')->first()->id;
        // dd($inviteid);
        $this->visit('requests')
             ->see($text)
             ->click("$inviteid");
             
        $this->type(33, 'hourly_price')
             ->type('Sample cover letter for placing a bid for my behat testing. Lorem Ipsum.','cover_letter')
             ->press('Submit');
             
        $this->see('Sample cover letter for placing a bid for my behat testing. Lorem Ipsum.');
        
        $this->visit('logout');

        $this->registerDummy('testProvider2', 'therapist');
        $this->visit('requests/'.$inviteid)
             ->type(22, 'hourly_price')
             ->type('Sample 22222 cover letter for placing a bid for my behat testing. Lorem Ipsum.','cover_letter')
             ->press('Submit');

        $this->see('Sample 22222 cover letter for placing a bid for my behat testing. Lorem Ipsum.')
             ->dontSee('Sample cover letter for placing a bid for my behat testing. Lorem Ipsum.');
        
        $this->visit('logout');

        // visiting login page doesn't allow you to use @ in email field so manual login required
        Auth::attempt(['email' => 'testClient@test.com', 'password' => 'password']);
        
        $this->visit('requests/'.$inviteid)
             ->see('Sample cover letter for placing a bid for my behat testing. Lorem Ipsum.')
             ->see('Sample 22222 cover letter for placing a bid for my behat testing. Lorem Ipsum.');
             
        $this->see('Close Request')
             ->press('Close Request');
        
        $this->asserttrue(Invitation::find($inviteid)->invitationStatus()->first()->description =='Closed');

        $this->see('This is my description to request for therapy. Please help me yo.')
             ->dontSee('Close Request')
             ->dontSee('Submit');
             
//      It no longer appears in the requests view
        $this->visit('requests')
             ->dontSee('This is my description to request for therapy. Please help me yo.');
             
        //Bidded providers can only see form/buttons on active request, not on closed requests 
        $this->visit('logout');
        Auth::attempt(['email' => 'testProvider2@test.com', 'password' => 'password']);
        $this->visit('requests/'.$inviteid)
             ->see('This is my description to request for therapy. Please help me yo.')
             ->dontSee('Withdraw Offer');
             
        // New providers cannot see form on closed request
        $this->visit('logout');
        $this->registerDummy('testTherapist3', 'therapist');
        $this->visit('requests/'.$inviteid)
             ->dontSee('This is my description to request for therapy. Please help me yo.')
             ->seePageIs('requests');
             
        $this->visit('logout');

        // Scenario: Providers tries to bid on own request but can't
        $this->registerDummy('testTherapist4', 'therapist');
        $this->makeInvitation('Request for therapy as a therapist. Please help me yo. Dummy Text Dummy Text Dummy Text Dummy Text');
        $inviteid2 = Invitation::whereDescription('Request for therapy as a therapist. Please help me yo. Dummy Text Dummy Text Dummy Text Dummy Text')->first()->id;

        $this->visit('requests')
             ->see('Request for therapy as a therapist.')
             ->click("$inviteid2");
             
        $this->see('Request for therapy as a therapist. Please help me yo. Dummy Text Dummy Text Dummy Text Dummy Text')
             ->dontSee('Make an offer to this request');
    }
    
    /**
     * Sorting and browsing requests
     * 
     */ 
    public function testBrowseRequests()
    {
        $this->registerAndRequest('testClient1', 'testClient1 making a request. Please help me');
        $this->registerAndRequest('testClient2', 'testClient2 making a request. Please help me');
        $this->registerAndRequest('testClient3', 'testClient3 making a request. Please help me');
        $this->registerAndRequest('testClient4', 'testClient4 making a request. Please help me');
        $this->registerAndRequest('testClient5', 'testClient5 making a request. Please help me');
        $this->registerAndRequest('testClient6', 'testClient6 making a request. Please help me');
        $this->registerAndRequest('testClient7', 'testClient7 making a request. Please help me');
        $this->registerAndRequest('testClient8', 'testClient8 making a request. Please help me');
        $this->registerAndRequest('testClient9', 'testClient9 making a request. Please help me');
        $this->registerAndRequest('testClient10', 'testClient10 making a request. Please help me');
        $this->registerAndRequest('testClient11', 'testClient11 making a request. Please help me');
        $this->registerAndRequest('testClient12', 'testClient12 making a request. Please help me');
        $this->registerAndRequest('testClient13', 'testClient13 making a request. Please help me');
        $this->registerAndRequest('testClient14', 'testClient14 making a request. Please help me');
        $this->registerAndRequest('testClient15', 'testClient15 making a request. Please help me');
        $this->registerAndRequest('testClient16', 'testClient16 making a request. Please help me');
        $this->registerAndRequest('testClient17', 'testClient17 making a request. Please help me');
        $this->registerAndRequest('testClient18', 'testClient18 making a request. Please help me');
        $this->registerAndRequest('testClient19', 'testClient19 making a request. Please help me');
        $this->registerAndRequest('testClient20', 'testClient20 making a request. Please help me');
        
        $this->registerDummy('testTherapist1', 'therapist');
        
        //   When I am on "requests?sortby=deadline&order=asc"
        //   Then I should see "first" request sorted by "deadline"
        $this->visit('requests?sortby=deadline&order=asc');
        $this->iShouldSeeOrDontSeeRequestSortedBy("first", "deadline", "see");
        $this->iShouldSeeOrDontSeeRequestSortedBy("last", "deadline", "dont");
        $this->visit('requests?sortby=deadline&order=desc');
        $this->iShouldSeeOrDontSeeRequestSortedBy("last", "deadline", "see");
        $this->iShouldSeeOrDontSeeRequestSortedBy("first", "deadline", "dont");       
        $this->visit('requests?sortby=budget_range_min&order=asc');
        $this->iShouldSeeOrDontSeeRequestSortedBy("first", "budget_range_min", "see");
        $this->iShouldSeeOrDontSeeRequestSortedBy("last", "budget_range_min", "dont");         
        $this->visit('requests?sortby=budget_range_min&order=desc');
        $this->iShouldSeeOrDontSeeRequestSortedBy("last", "budget_range_min", "see");       
        $this->iShouldSeeOrDontSeeRequestSortedBy("first", "budget_range_min", "dont");
        
    }
    
    private function iShouldSeeOrDontSeeRequestSortedBy($arg1, $sort_by, $seeOrDont)
    {
        if ($arg1 == "first")
        {
            $desc = Invitation::where('description', 'LIKE', 'testClient%')->orderBy($sort_by, 'asc')->first()->description;
        } elseif ($arg1 == "last")
        {
            $desc = Invitation::where('description', 'LIKE', 'testClient%')->orderBy($sort_by, 'desc')->first()->description;
        }
    
        if ($seeOrDont=="see") {
            $this->see($desc);
        } else {
            $this->dontSee($desc);
        }
    }

}

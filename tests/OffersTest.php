<?php

use App\Offer;
use App\Invitation;

class OffersTest extends TestCase
{
    /**
     * View my existing offers
     *
     * @return void
     */
    public function testViewExistingOffers()
    {
        $this->registerAndRequest('testClient1', 'Fighting for help! Help me please');
        $this->registerAndRequest('testClient2', 'Fighting for second help! Help me please');
        
        $inviteid = Invitation::whereDescription('Fighting for help! Help me please')->first()->id;
        $this->registerDummy('testTherapist1', 'therapist');
        $this->visit('requests')
             ->see('Fighting for help! Help me please')
             ->click($inviteid);
        $this->type(44, 'hourly_price')
             ->type('Sample cover letter 1 Lorem Ipsum.','cover_letter')
             ->press('Submit');
             
        $inviteid = Invitation::whereDescription('Fighting for second help! Help me please')->first()->id;
        $this->visit('requests')
             ->see('Fighting for second help! Help me please')
             ->click($inviteid);
        $this->type(44, 'hourly_price')
             ->type('Sample cover letter 2 Lorem Ipsum.','cover_letter')
             ->press('Submit');
             
        $this->visit('offers')
             ->see('Sample cover letter 1 Lorem Ipsum.')
             ->see('Sample cover letter 2 Lorem Ipsum.')
             ->see('2</strong> offer(s) listed');

        // View specific offer
        $offerid = Offer::where('cover_letter', 'Sample cover letter 1 Lorem Ipsum.')->first()->id;
        $this->visit('offers/'.$offerid)
             ->see('Sample cover letter 1 Lorem Ipsum.');
             
        $offerid = Offer::where('cover_letter', 'Sample cover letter 2 Lorem Ipsum.')->first()->id;
        $this->visit('offers/'.$offerid)
             ->see('Sample cover letter 2 Lorem Ipsum.');
        
        // attempt to view another users offer     
        $this->visit('logout');
        
        $this->registerDummy('testTherapist3123', 'therapist');
        $this->visit('requests')
             ->see('Fighting for second help! Help me please')
             ->click($inviteid);
        $this->dontSee('Sample cover letter 2 Lorem Ipsum.');
        $this->visit('offers/'.$offerid)
             ->dontSee('Sample cover letter 2 Lorem Ipsum.')
             ->seePageIs('profile');
             
        //  Attempt to view offers while not logged in and fail
        $this->visit('logout')
             ->visit('offers')
             ->seePageIs('login');
    }
}


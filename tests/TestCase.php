<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Question;
use Carbon\Carbon;

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    use DatabaseTransactions;
        
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }
    
    /**
     * Register a dummy client account, only the route would depend on the type of user being registered, i.e. a client or a therapist
     * 
     * @return void
     * @param string @name name of user
     * @param string @type client or therapist
     */ 
    protected function registerDummy($name, $type='client', $qs = [])
    {
        if ($type=='therapist') {
            $this->visit('/registerprovider');
        } else {
            $this->visit('/register');
        }
        
        $this->type($name, 'name')
            ->type("$name@test.com", 'email')
            ->type('password', 'password')
            ->type('password', 'password_confirmation')
            ->press('Register');
            
        if ($type=='therapist') {
            $this->registerTherapistQuestions($qs);
        } 
    }
    
    private function registerTherapistQuestions($questions)
    {
        $this->visit('providerprofile')
            ->see('about my practice');

        $inputs['account_summary'] = 'random text for account summary. lorem ipsum dolor, nunavat is called that because nonofit is inhabitable.';   
        $inputs['first_session_rate'] = '20'; 
        $inputs['first_session_length'] = '30';
        $inputs['standard_hourly_rate_min'] = '30';
        $inputs['standard_hourly_rate_max'] = '60';
        $inputs['terms'] = true;
        
        
        //randomly select some answers
        foreach ($questions as $question) {
            $answerid = $question->answers()->orderByRaw("RAND()")->first()->id;
            $inputs["$question->id"] = "$answerid";
        }

        $this->submitForm('Submit', $inputs);
    }
        
    protected function registerAndRequest($user, $desc)
    {
        $this->registerDummy($user, 'client');
        $this->makeInvitation($desc);        
        $this->visit('logout');
    }
        
    /**
     * protected function to make a request
     * 
     */
    protected function makeInvitation($desc)
    {
        $dt = new Carbon();
        $dt = $dt->addDays(rand(1,13))->format('Y-m-d');
        
        $this->visit('requests/create')
             ->type($desc, 'description')
             ->type($dt, 'deadline')
             ->type(rand(1,50), 'budget_range_min')
             ->type(rand(51,200), 'budget_range_max')
             ->press('Submit');
        
    }
}

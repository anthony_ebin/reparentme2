<?php

class HomePageTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('The online marketplace for emotional health');
    }

    public function testViewsDashboard()
    {
        $this->registerDummy('testClient', 'client');
        
        $this->visit('profile')
             ->see('Active</b> Requests')
             ->see('Request History')
             ->dontSee('My bids');
             
        $this->visit('logout');
        
        $this->registerDummy('testTherapist', 'therapist');
        
        $this->visit('profile')
             ->see('Active</b> Requests')
             ->see('Request History')
             ->see('My bids');
    }
}

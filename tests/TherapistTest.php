<?php

use App\Question;
use App\User;
use App\Therapist;

class TherapistTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testSignUpAsTherapist()
    {
        // url for register as therapist goes to auth controller's registerTherapist function
        $this->registerDummy('testProvider', 'therapist');
        
        $this->seePageIs('/requests');
    }
    
    /**
     * Create some dummy therapists and dummy clients, see if matching works as expected.
     * 
     * @return void
     */ 
     public function testMatching()
     {
        $questions = Question::all();
        
        // create therapist 1
        $this->registerDummy('testTherapist1', 'therapist', $questions);
         
        // verify therapist data is in db
        $therapist = User::whereName('testTherapist1')->first()->therapist()->first();
        // dd(DB::select(DB::raw("select * from answer_therapist where therapist_id = $therapist->id")));
        $this->seeInDatabase('therapists', ['user_id' => $therapist->user_id]);
        $this->seeInDatabase('answer_therapist', ['therapist_id' => $therapist->id]);
            
        // logout
        $this->visit('logout');

        // create client 1 with a random profile
        $this->registerDummy('testClient1', 'client');
        
        $this->seeInDatabase('users', ['name' => 'testClient1']);
        
        $this->visit('profile');
        
        
        //select first and second answer for each question. There is a bug with the crawler that it does not allow me to check multiple checkboxes.
        foreach ($questions as $question) {
            $answerid1 = $question->answers()->first()->id;
            $answerid2 = $question->answers()->skip(1)->first()->id;
            
            if ($question->checkbox) {
                $inputs["$question->id[0]"] = "$answerid1";
                $inputs["$question->id[1]"] = "$answerid2";
            } else // if question is a radio button
            {
                $inputs["$question->id"] = "$answerid1";
            }
            // select importance per question
            $inputs["$question->id"."_importance"] = "very";
        }
        
        // dd($inputs);
        $this->submitForm('Submit', $inputs);
        unset($inputs); // clear array or it will be reused below
        
        // If client accepts "Any" answer then importance is 0 and selected importance doesn't matter
         
         // check client 1 score against therapist 1 in matches table/page
         // get client 1 score
        $client = User::whereName('testClient1')->first();
        $this->seeInDatabase('answer_user', ['user_id' => $client->id]);
        $this->seeInDatabase('answer_user', ['importance' => '50']);

        //  Get total score: for each question get sum of importance from per client. Expected 600 for 12 questions which are very important
        $totalscore = DB::select(DB::raw('select sum(importance) as imp from (select importance 
                                                            from answer_user 
                                                            inner join answers on answers.id = answer_user.answer_id
                                                            where user_id= :clientid group by question_id) as distinctAnswers'), ['clientid'=>$client->id])[0]->imp;
        // select sum(importance) from (select importance from answer_user inner join answers on answers.id = answer_user.answer_id where user_id=7 group by question_id) as distinctAnswers;
        
        // Find one question that matches therapist 1 & client 1
        $score1 = $this->getScore($client, $therapist);
        
        $testscore= sprintf("%.2f%%", ($score1/$totalscore) * 100);
        // dd('total'.$totalscore.'score'.$score1);
        //check if score is same in view
        $this->visit('provider')
            ->see($testscore);
        
        // print_r(DB::select(DB::raw('select * from answer_therapist where therapist_id = ?'), [$therapist->id]));
        
        // Change/delete one therapist answer that matches the client, this should drop the therapist score by 50 
        DB::delete('delete from answer_therapist where therapist_id = ? AND answer_id IN (select answer_id from answer_user where user_id = ? AND importance <> 0) LIMIT 1', [$therapist->id, $client->id]);
        
        // print_r(DB::select(DB::raw('select * from answer_therapist where therapist_id = ?'), [$therapist->id]));
        // print_r(DB::select(DB::raw('select * from answer_user where user_id = ?'), [$client->id]));
        
        //refreshing models else score 2 will be same as score 1
        $client = User::whereName('testClient1')->first();
        $therapist = User::whereName('testTherapist1')->first()->therapist()->first();
        
        $score2 = $this->getScore($client, $therapist);
        
        // print_r($score2. $score1);
        
        $this->asserttrue($score1==($score2+50));
        
        $testscore = sprintf("%.2f%%", (($score2)/$totalscore) * 100);
        //check if score is same in view
        $this->visit('provider')
            ->see($testscore);

         // create therapist 2
         // create therapist 3
         // create client 2
         // create client 3
     }

    private function getScore($client, $therapist)
    {
        $score = 0;
        foreach ($client->answers as $clientAnswer) {
            foreach ($therapist->answers as $therapistAnswer) {
                if($therapistAnswer->pivot->answer_id==$clientAnswer->pivot->answer_id)
                {
                    $score += $clientAnswer->pivot->importance;
                }
            }
        }
        
        return $score;
    }

    /**
     * When guest visits providerprofile then he shouldn't see the page
     */
    public function testLogins()
    {
        
        $this->visit('logout')
            ->visit('providerprofile')
            ->dontSee('about my practice')
            ->see('Forgot Your Password'); //redirect to login
        
    }
    
    /**
     * User should see a link to become a provider unless he is already a provider
     */
     public function testProviderSignsUp()
     {
         $this->visit('home')
              ->see('Become a provider');
         
         $this->registerDummy('testTherapist', 'therapist');
         
         $this->visit('profile')
              ->dontSee('Become a provider');

     }

    /**
     * User should not be allowed to become a provider if he fills an incomplete form
     */
     public function testProviderDoesntFillForm()
     {
        $this->registerDummy('testTherapistYo', 'client');
         
        $this->visit('providerprofile')
             ->type('random text for account summary. lorem ipsum dolor, nunavat is called that because nonofit is inhabitable.', 'account_summary')
             ->type('20', 'first_session_rate')
             ->type('30', 'standard_hourly_rate_min')
             ->type('60', 'standard_hourly_rate_max');

        $this->press('Submit');
        
        $this->see('issues with your submission');
        
        //check that therapist is not created in DB
        $row = DB::select(DB::raw('select * from therapists inner join users on users.id = therapists.user_id where name = :name'), ['name'=>'testTherapistYo']);
        $this->asserttrue(count($row)==0);
     }  
     
    /**
     * User should not be allowed to become a provider if he doesn't agree to terms
     */
     public function testProviderDisagrees()
     {
        $this->registerDummy('testTherapist', 'client');
         
        $this->visit('providerprofile')
             ->type('random text for account summary. lorem ipsum dolor, nunavat is called that because nonofit is inhabitable.', 'account_summary')
             ->type('20', 'first_session_rate')
             ->type('30', 'first_session_length')
             ->type('30', 'standard_hourly_rate_min')
             ->type('60', 'standard_hourly_rate_max')
             ->uncheck('terms');
             
        $this->press('Submit');
             
             //did not select agree
        $this->see('issues with your submission');
             
     }
     
    /**
     * Client visiits his profile and can see previous answers - can't test
     * Therapist visit his profile and can see previous answers
     */
     public function testUserEditProfiles()
     {
        $this->registerDummy('testTherapist', 'client');
         
        $this->visit('providerprofile')
             ->type('random text for account summary. lorem ipsum dolor, nunavat is called that because nonofit is inhabitable.', 'account_summary')
             ->type('20', 'first_session_rate')
             ->type('30', 'first_session_length')
             ->type('30', 'standard_hourly_rate_min')
             ->type('60', 'standard_hourly_rate_max')
             ->check('terms');
             
        $this->press('Submit');
        
        $this->visit('home')
            ->visit('providerprofile')
            ->see('random text for account summary. lorem ipsum dolor, nunavat is called that because nonofit is inhabitable.');
            
        // check that only one record is created per therapist
        $this->visit('providerprofile')
             ->type('random text for account summary. lorem ipsum dolor, nunavat is called that because nonofit is inhabitable.', 'account_summary')
             ->type('20', 'first_session_rate')
             ->type('30', 'first_session_length')
             ->type('30', 'standard_hourly_rate_min')
             ->type('60', 'standard_hourly_rate_max')
             ->check('terms');
             
        $this->press('Submit');
        $this->asserttrue(count(DB::select(DB::raw('select therapists.id from therapists inner join users on users.id = therapists.user_id where name = :therapistname'), ['therapistname'=>'testTherapist']))==1);
        
        // test that duplicate answers are not created for therapists
        $this->visit('providerprofile');
        //select answer 1 for question 1
        $question = Question::first();
        $answerid = $question->answers()->first()->id;
        $inputs["$question->id"] = "$answerid";
        $this->submitForm('Submit', $inputs);

        // do it again and make sure there is only one line in DB
        $this->visit('providerprofile');
        $inputs["$question->id"] = "$answerid";
        $this->submitForm('Submit', $inputs);
        
        $therapistid = Auth::user()->therapist()->first()->id;
        $this->asserttrue(count(DB::select(DB::raw('select * from answer_therapist where therapist_id = :therapistid and answer_id = :answerid'), ['therapistid'=>$therapistid,'answerid'=>$answerid])) == 1);
        unset($inputs);
        
        //-------------------
        // test that duplicate answers are not created for clients
        $this->visit('logout');
        $this->registerDummy('testClient111', 'client');
        
        $this->visit('profile');
        
        if ($question->checkbox) {
            $inputs["$question->id[0]"] = "$answerid";
        } else // if question is a radio button
        {
            $inputs["$question->id"] = "$answerid";
        }

        // select importance per question
        $inputs["$question->id"."_importance"] = "very";
        $this->submitForm('Submit', $inputs);
        
        // duplicate
        $this->visit('profile');
        if ($question->checkbox) {
            $inputs["$question->id[0]"] = "$answerid";
        } else // if question is a radio button
        {
            $inputs["$question->id"] = "$answerid";
        }

        // select importance per question
        $inputs["$question->id"."_importance"] = "very";
        $this->submitForm('Submit', $inputs);
        
        $clientid = Auth::user()->id;
        $this->asserttrue(count(DB::select(DB::raw('select * from answer_user where user_id = :userid and answer_id = :answerid'), ['userid'=>$clientid,'answerid'=>$answerid])) == 1);

     }

    /**
     * Register 3 therapists
     * Logout and visit provider browse page, you should see all three with their details
     */
     public function testBrowseProviders()
     {
        $this->registerDummy('testTherapist1', 'client');
         
        $this->visit('providerprofile')
             ->type('testTherapist1profileSummaryYoTestThisShit.', 'account_summary')
             ->type('20', 'first_session_rate')
             ->type('30', 'first_session_length')
             ->type('30', 'standard_hourly_rate_min')
             ->type('60', 'standard_hourly_rate_max')
             ->check('terms');
             
        $this->press('Submit');
        $this->visit('logout');
        
        $this->registerDummy('testTherapist2', 'client');
         
        $this->visit('providerprofile')
             ->type('testTherapist2profileSummaryYoTestThisShit.', 'account_summary')
             ->type('21', 'first_session_rate')
             ->type('31', 'first_session_length')
             ->type('31', 'standard_hourly_rate_min')
             ->type('61', 'standard_hourly_rate_max')
             ->check('terms');
             
        $this->press('Submit');
        $this->visit('logout');
        
        $this->registerDummy('testTherapist3', 'client');
         
        $this->visit('providerprofile')
             ->type('testTherapist3profileSummaryYoTestThisShit.', 'account_summary')
             ->type('22', 'first_session_rate')
             ->type('32', 'first_session_length')
             ->type('32', 'standard_hourly_rate_min')
             ->type('62', 'standard_hourly_rate_max')
             ->check('terms');
             
        $this->press('Submit');
        $this->visit('logout');
        
        $this->visit('provider')
             ->see('testTherapist1profileSummaryYoTestThisShit')
             ->see('testTherapist2profileSummaryYoTestThisShit')
             ->see('testTherapist3profileSummaryYoTestThisShit')
             ->see('20')
             ->see('21')
             ->see('22')
             ->see('30')
             ->see('31')
             ->see('32')
             ->see('60')
             ->see('61')
             ->see('62');
        
        $id = User::whereName('TestTherapist3')->first()->id;
             
        $this->click('TestTherapist3')
             ->seePageIs('provider/'.$id.'-testtherapist3')
             ->see('testTherapist3profileSummaryYoTestThisShit')
             ->see('22')
             ->see('32')
             ->see('62');
         
     }


}

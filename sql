SELECT 
    name,
    first_session_rate, 
    first_session_length, 
    standard_hourly_rate_min, 
    standard_hourly_rate_max,
    scores.score
FROM therapists 
    INNER JOIN users on users.id=therapists.user_id
    LEFT JOIN (SELECT
                    SUM(importance)/155 as score,
                    answer_therapist.therapist_id as therapist_id
                FROM answer_user
                    INNER JOIN answers on answers.id = answer_user.answer_id
                    INNER JOIN answer_therapist on answers.id = answer_therapist.answer_id 
                WHERE answer_user.user_id = 99
                GROUP BY answer_therapist.therapist_id) scores on scores.therapist_id =therapists.id
GROUP BY users.id;